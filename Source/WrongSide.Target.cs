// Created by Dmitriy Chapurkin. All assets are used for educational purposes

using UnrealBuildTool;
using System.Collections.Generic;

public class WrongSideTarget : TargetRules
{
	public WrongSideTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V4;

		ExtraModuleNames.AddRange( new string[] { "WrongSide" } );
	}
}
