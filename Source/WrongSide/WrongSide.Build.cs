// Created by Dmitriy Chapurkin. All assets are used for educational purposes

using UnrealBuildTool;

public class WrongSide : ModuleRules
{
	public WrongSide(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput", "GameplayAbilities", "ModelViewViewModel", "FieldNotification" });

		PrivateDependencyModuleNames.AddRange(new string[] { "NavigationSystem", "GameplayTags", "AnimGraphRuntime", "PhysicsCore", "AIModule", "GameplayTasks", "MotionWarping" });

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
