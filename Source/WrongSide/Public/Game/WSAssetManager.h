// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "Engine/AssetManager.h"
#include "WSAssetManager.generated.h"

UCLASS(Abstract, Blueprintable)
class WRONGSIDE_API UWSAssetManager : public UAssetManager
{
	GENERATED_BODY()

public:
	static UWSAssetManager* Get();

	virtual void StartInitialLoading() override;
};
