// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "WSGameModeBase.generated.h"

UCLASS(Abstract)
class WRONGSIDE_API AWSGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UFUNCTION(Blueprintable, Category="WrongSide")
	void MovePlayerToPlayerStart(const FString& PlayerStartTag);

protected:
	UPROPERTY(EditDefaultsOnly, Category="WrongSide")
	FString StartSpotName = "StartGame";

	virtual bool UpdatePlayerStartSpot(AController* Player, const FString& Portal, FString& OutErrorMessage) override;
};
