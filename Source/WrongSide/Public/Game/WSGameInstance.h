// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "WSGameInstance.generated.h"

UCLASS(Abstract)
class WRONGSIDE_API UWSGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	virtual void Init() override;

	UFUNCTION(Blueprintable, Category="WrongSide")
	void StartNewGame();

	UFUNCTION(Blueprintable, Category="WrongSide")
	void LoadGameFromSlot(const FString& InSlotName);

	UTexture2D* GetMapLoadingImage(const FName& MapName = NAME_None) const;

	UFUNCTION(Blueprintable, Category="WrongSide")
	void MovePlayerTo(const FString& PlayerStartTag, const FName LevelName = NAME_None);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="WrongSide")
	FName MainMenuMapName = "MainMenuLevel";

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="WrongSide")
	FName NewGameMapName = NAME_None;

	//TODO �������� �� ��� ������ ������� �� �������
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="WrongSide")
	TObjectPtr<UDataTable> MapsInfoDataTable;

private:
	FString CurrentPlayerStartTag;		

	void MovePlayerToPlayerStart() const;
};
