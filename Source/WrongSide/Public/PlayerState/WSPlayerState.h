// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "SaveGame/SavableActorInterface.h"
#include "WSPlayerState.generated.h"

class UWSAttributeSet;
class UWSAbilitySystemComponent;
UCLASS(Abstract)
class WRONGSIDE_API AWSPlayerState : public APlayerState, public ISavableActorInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="WrongSide|Player")
	void LevelUp();

	UFUNCTION(BlueprintCallable, Category="WrongSide|Player")
	void AddXP(const int32 XPToAdd);

	UFUNCTION(BlueprintPure, Category="WrongSide|Player")
	int32 GetPlayerLevel() const { return Level; }

	UFUNCTION(BlueprintPure, Category="WrongSide|Player")
	int32 GetPlayerXP() const { return XP; }

	AWSPlayerState();

	UWSAbilitySystemComponent* GetAbilityComponent() const { return AbilityComponent; }

	UWSAttributeSet* GetAttributeSet() const { return AttributeSet; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="WrongSide")
	TObjectPtr<UWSAbilitySystemComponent> AbilityComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="WrongSide")
	TObjectPtr<UWSAttributeSet> AttributeSet;

private:
	UPROPERTY(SaveGame)
	int32 Level = 1;

	UPROPERTY(SaveGame)
	int32 XP = 0;
};
