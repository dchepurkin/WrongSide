// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "WSFunctionLibrary.generated.h"

class UWSInteractionComponent;
class IWSInteractionInterface;
class UWSInteractionInterface;
class IWSPlayerInterface;

UCLASS()
class WRONGSIDE_API UWSFunctionLibrary : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintPure, Category="WrongSide")
    static bool IsEnemy(AActor* InActor);

    UFUNCTION(BlueprintCallable, Category="WrongSide", meta=(DefaultToSelf="InActor"))
    static void SetActorType(AActor* InActor, UPARAM(meta=(Categories="Actor.Type"))FGameplayTag Type);

    UFUNCTION(BlueprintCallable, Category="WrongSide")
    static void WarpToLocation(AActor* InActor, const FVector& InLocation);

    UFUNCTION(BlueprintCallable, Category="WrongSide")
    static void SetInteractionLoopAnimation(AActor* InActor, AActor* InteractedActor);

    UFUNCTION(BlueprintCallable, Category="WrongSide")
    static void BreakInteractionLoopAnimation(AActor* InActor);

    UFUNCTION(BlueprintCallable, Category="WrongSide")
    static void SetOutlineEnabled(AActor* InActor, bool IsEnabled);

    UFUNCTION(BlueprintPure, Category="WrongSide")
    static UWSInteractionComponent* GetPlayerInteractionComponent(AActor* InPlayerActor);

    UFUNCTION(BlueprintCallable, Category="WrongSide")
    static void SetPlayerFocusEnabled(AActor* InPlayerActor, bool IsEnabled);

    UFUNCTION(BlueprintPure, Category="WrongSide")
    static AActor* GetPlayerFocusedActor(AActor* InPlayerActor);
};
