// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "InputAction.h"
#include "WSInputAction.generated.h"

UCLASS()
class WRONGSIDE_API UWSInputAction : public UInputAction
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category="WrongSide", meta=(Categories="Input"))
	FGameplayTag InputTag;

protected:
	UPROPERTY(EditAnywhere, Category="WrongSide")
	bool bExecuteIfDead = false;
};
