#pragma once

#include "CoreMinimal.h"
#include "SaveGameTypes.generated.h"

USTRUCT()
struct FSaveData
{
	GENERATED_BODY()

	UPROPERTY()
	FName Name{};

	UPROPERTY()
	TArray<uint8> ByteData;
};

USTRUCT()
struct FComponentSaveData : public FSaveData
{
	GENERATED_BODY()

	UPROPERTY()
	FTransform Transform = FTransform::Identity;
};

USTRUCT()
struct FActorSaveData : public FComponentSaveData
{
	GENERATED_BODY()

	UPROPERTY()
	TSubclassOf<AActor> Class = nullptr;

	UPROPERTY()
	TArray<FComponentSaveData> ComponentsData{};
};

USTRUCT()
struct FPlayerSaveData : public FActorSaveData
{
	GENERATED_BODY()

	UPROPERTY()
	FRotator ControlRotation = FRotator::ZeroRotator;

	UPROPERTY()
	TEnumAsByte<EMovementMode> MovementMode = MOVE_Walking;
};

USTRUCT()
struct FLevelSaveData
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<FActorSaveData> LevelData;
};

USTRUCT(BlueprintType)
struct FSaveInfo
{
	GENERATED_BODY()

	FSaveInfo() {}

	explicit FSaveInfo(const FString& InSlotName, const TArray<FColor>& ScreenshotData)
		: SlotName(InSlotName),
		  DateTime(FDateTime::UtcNow())
	{
		const int32 BufferSize = ScreenshotData.Num() * 4;
		Screenshot.Reserve(BufferSize);
		Screenshot.AddUninitialized(BufferSize);
		FMemory::Memcpy(Screenshot.GetData(), ScreenshotData.GetData(), BufferSize);
	}

	//Имя слота сохранения
	UPROPERTY(BlueprintReadOnly)
	FString SlotName;

	UPROPERTY(BlueprintReadOnly)
	FDateTime DateTime;

	UPROPERTY()
	TArray<uint8> Screenshot;
};
