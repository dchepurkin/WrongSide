// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "SaveGameTypes.h"
#include "GameFramework/SaveGame.h"
#include "SaveGameData.generated.h"

UCLASS()
class WRONGSIDE_API USaveGameData : public USaveGame
{
	GENERATED_BODY()

public:
	bool Save(const FString& InSlotName, const FName& InLevelName, const FPlayerSaveData& InPlayerSaveData,
	          const TMap<FName, FLevelSaveData>& InWorldSaveData);

	const FPlayerSaveData& GetPlayerSaveData() const { return PlayerSaveData; }

	const TMap<FName, FLevelSaveData>& GetWorldSaveData() const { return WorldSaveData; }

	const FName& GetLevelName() const { return LevelName; }

protected:
	UPROPERTY()
	FName LevelName;

	UPROPERTY()
	FPlayerSaveData PlayerSaveData;

	UPROPERTY()
	TMap<FName, FLevelSaveData> WorldSaveData;
};
