// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ScreenshotMaker.generated.h"

UCLASS(NotBlueprintable)
class WRONGSIDE_API AScreenshotMaker : public AActor
{
	GENERATED_BODY()

public:
	AScreenshotMaker();
	
	bool CreateScreenshot(const FVector2D& Resolution, TArray<FColor>& ColorData);

protected:
	UPROPERTY(EditDefaultsOnly)
	TObjectPtr<USceneCaptureComponent2D> Camera;

private:
	bool SetCameraToPlayerView();
};
