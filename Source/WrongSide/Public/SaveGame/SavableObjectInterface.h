// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "SavableObjectInterface.generated.h"

UINTERFACE(MinimalAPI)
class USavableObjectInterface : public UInterface
{
	GENERATED_BODY()
};

class WRONGSIDE_API ISavableObjectInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, Category="WrongSide|SaveGame")
	void OnGameLoaded();

	virtual void OnGameLoaded_Implementation() { }
};
