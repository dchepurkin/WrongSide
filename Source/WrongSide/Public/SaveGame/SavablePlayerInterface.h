﻿// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "SavableObjectInterface.h"
#include "SavablePlayerInterface.generated.h"

DEFINE_LOG_CATEGORY_STATIC(LogISavablePlayer, All, All);

UINTERFACE()
class USavablePlayerInterface : public USavableObjectInterface
{
	GENERATED_BODY()
};

class WRONGSIDE_API ISavablePlayerInterface : public ISavableObjectInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, Category="WrongSide|SaveGame")
	void OnPlayerLoaded();

	virtual void OnPlayerLoaded_Implementation() { }
};
