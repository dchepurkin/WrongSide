// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "SavableObjectInterface.h"
#include "UObject/Interface.h"
#include "SavableActorInterface.generated.h"

DEFINE_LOG_CATEGORY_STATIC(LogISavableActor, All, All);

UINTERFACE(MinimalAPI)
class USavableActorInterface : public USavableObjectInterface
{
	GENERATED_BODY()
};

class WRONGSIDE_API ISavableActorInterface : public ISavableObjectInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, Category="WrongSide|SaveGame")
	void OnActorLoaded();

	virtual void OnActorLoaded_Implementation() { }
};
