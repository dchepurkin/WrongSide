// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "SaveGameTypes.h"
#include "GameFramework/SaveGame.h"
#include "SaveGameInfo.generated.h"

UCLASS()
class WRONGSIDE_API USaveGameInfo : public USaveGame
{
	GENERATED_BODY()

public:
	bool Save(const FString& InSlotName, const TArray<FColor>& Screenshot);

	bool Remove(const FString& InSlotName);

	void GetSlotsInfo(TArray<FSaveInfo>& Slots) const { Slots = SaveGamesInfo; }

private:
	UPROPERTY()
	TArray<FSaveInfo> SaveGamesInfo;
};
