// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "SavableObjectInterface.h"
#include "UObject/Interface.h"
#include "SavableComponentInterface.generated.h"

DEFINE_LOG_CATEGORY_STATIC(LogISavableComponent, All, All);

UINTERFACE(MinimalAPI)
class USavableComponentInterface : public USavableObjectInterface
{
	GENERATED_BODY()
};

class WRONGSIDE_API ISavableComponentInterface : public ISavableObjectInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, Category="WrongSide|SaveGame")
	void OnComponentLoaded();

	virtual void OnComponentLoaded_Implementation() { }
};
