// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "SavableComponentInterface.h"
#include "SaveGameTypes.h"
#include "Kismet/GameplayStatics.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "SaveGameManager.generated.h"

class USaveGameData;
class USaveGameInfo;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameSavedSignature);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameLoadedSignature);

UCLASS(Config="SaveGame", DefaultConfig, DisplayName="Save Game Manager")
class WRONGSIDE_API USaveGameManager : public UGameInstanceSubsystem
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="WrongSide|SaveGame")
    FOnGameSavedSignature OnGameSaved;

    UPROPERTY(BlueprintAssignable, Category="WrongSide|SaveGame")
    FOnGameLoadedSignature OnGameLoaded;

    UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category="WrongSide|SaveGame")
    FVector2D ScreenshotResolution{160, 90};

    //USubsystem interface
    virtual void Initialize(FSubsystemCollectionBase& Collection) override;
    //~USubsystem interface

    UFUNCTION(BlueprintCallable, Category="WrongSide|SaveGame")
    void CreateAutoSave();

    UFUNCTION(BlueprintCallable, Category="WrongSide|SaveGame")
    void LoadAutoSave();

    UFUNCTION(BlueprintCallable, Category="WrongSide|SaveGame")
    void RemoveAutoSave();

    UFUNCTION(BlueprintCallable, Category="WrongSide|SaveGame")
    bool SaveGameToSlot(const FString& InSlotName);

    UFUNCTION(BlueprintCallable, Category="WrongSide|SaveGame")
    void RemoveSaveGameInSlot(const FString& InSlotName);

    UFUNCTION(BlueprintCallable, Category="WrongSide|SaveGame")
    void LoadGameFromSlot(const FString& InSlotName);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="WrongSide|SaveGame")
    void GetSlots(TArray<FSaveInfo>& Slots);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="WrongSide|SaveGame")
    bool HaveAnySavedGames();

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="WrongSide|SaveGame")
    UTexture2D* GetScreenshot(const FSaveInfo& SlotInfo);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="WrongSide|SaveGame")
    FName GetLevelNameInSlot(const FString& InSlotName) const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="WrongSide|SaveGame")
    static bool IsAlreadySavedInSlot(const FString& InSlotName);

    static USaveGameManager* GetSaveGameManager() { return SaveGameManager; }

private:
    FString LoadingSlotName;

    //SAVE DATA
    UPROPERTY()
    USaveGameInfo* SaveGameInfoCache;

    UPROPERTY()
    FPlayerSaveData PlayerSaveData;

    UPROPERTY()
    TMap<FName, FLevelSaveData> WorldSaveData;
    //SAVE DATA

    UPROPERTY()
    TArray<UObject*> LoadedObjects;

    inline static USaveGameManager* SaveGameManager;

    void GetSaveData(FLevelSaveData& OutLevelData, FPlayerSaveData& OutPlayerData);

    void LoadSaveData(const bool IsAutoSaveData);

    void GetActorData(AActor* InActor, FActorSaveData& OutData);

    void LoadActorData(AActor* InActor, const FActorSaveData& InData);

    void GetPlayerData(AActor* InPlayerActor, FPlayerSaveData& OutData);

    void LoadPlayerData(AActor* InPlayerActor, const FPlayerSaveData& InData, bool IsAutoSaveData);

    void GetByteData(UObject* InObject, FSaveData& OutData) const;

    void LoadByteData(UObject* InObject, const FSaveData& InData) const;

    void CreateScreenshot(TArray<FColor>& OutScreenshot) const;

    USaveGameInfo* GetSaveGameSlotsInfo();

    USaveGameData* GetSaveGameSlot(const FString& InSlotName) const;

    FName GetCurrentLevelName() const;

    //~Begin Global Delegates Callbacks
    void PreLoadMap_Callback(const FString& LoadedLevelName);

    void PostLoadMap_Callback(UWorld* LoadedWorld);
    //~End Global Delegates Callbacks

    template<typename FunctionType>
    void IterateActorComponents(AActor* InAtor, FunctionType InFunc) const
    {
        for(UActorComponent* Component : InAtor->GetComponents())
        {
            if(Component && Component->GetClass()->ImplementsInterface(USavableComponentInterface::StaticClass()))
            {
                InFunc(Component);
            }
        }
    }

    template<typename DataType>
    void GetComponentsData(AActor* InActor, DataType& InData)
    {
        FComponentSaveData ComponentSaveData;
        this->IterateActorComponents(InActor, [&](UActorComponent* InComponent)
        {
            this->GetByteData(InComponent, ComponentSaveData);
            ComponentSaveData.Name = InComponent->GetFName();

            if(const auto SceneComponent = Cast<USceneComponent>(InComponent))
            {
                ComponentSaveData.Transform = SceneComponent->GetRelativeTransform();
            }

            InData.ComponentsData.Add(ComponentSaveData);
        });
    }

    template<typename DataType>
    void LoadComponentsData(AActor* InActor, const DataType& InData)
    {
        this->IterateActorComponents(InActor, [&](UActorComponent* InComponent)
        {
            const FComponentSaveData* FoundComponentData = InData.ComponentsData.FindByPredicate([InComponent](const FComponentSaveData& ComponentData)
            {
                return ComponentData.Name == InComponent->GetFName();
            });
            if(!FoundComponentData) return;

            this->LoadByteData(InComponent, *FoundComponentData);
            if(const auto SceneComponent = Cast<USceneComponent>(InComponent))
            {
                SceneComponent->SetRelativeTransform(FoundComponentData->Transform);
            }

            this->LoadedObjects.Add(InComponent);
            ISavableComponentInterface::Execute_OnComponentLoaded(InComponent);
        });
    }

    template<class SaveGameObjectClass>
    SaveGameObjectClass* GetSaveGameObject(const FString& InSlotName) const
    {
        return UGameplayStatics::DoesSaveGameExist(InSlotName, 0)
                   ? Cast<SaveGameObjectClass>(UGameplayStatics::LoadGameFromSlot(InSlotName, 0))
                   : Cast<SaveGameObjectClass>(UGameplayStatics::CreateSaveGameObject(SaveGameObjectClass::StaticClass()));
    }
};
