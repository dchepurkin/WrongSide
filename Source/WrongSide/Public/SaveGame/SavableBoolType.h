﻿#pragma once

#include "CoreMinimal.h"
//#include "SavableBoolType.generated.h"

DEFINE_LOG_CATEGORY_STATIC(LogSavableBool, All, All);

UENUM(BlueprintType)
enum class ESavableBool : uint8
{
	None = 0 UMETA(Hidden, DisplayName="False"),
	EFalse = 1 UMETA(DisplayName="False"),
	ETrue = 2 UMETA(DisplayName="True")
};

//Оператор преобразования в bool
inline bool operator&(const ESavableBool& SavableBool)
{
	return SavableBool == ESavableBool::ETrue;
}

inline ESavableBool& operator&=(ESavableBool& SavableBool, bool Bool)
{
	SavableBool = Bool ? ESavableBool::ETrue : ESavableBool::EFalse;
	return SavableBool;
}

inline bool operator==(const ESavableBool& SavableBool, bool Bool)
{
	return &SavableBool == Bool;
}

inline bool operator==(bool Bool, const ESavableBool& SavableBool)
{
	return SavableBool == Bool;
}

inline bool operator!=(const ESavableBool& SavableBool, bool Bool)
{
	return &SavableBool != Bool;
}

inline bool operator!=(bool Bool, const ESavableBool& SavableBool)
{
	return &SavableBool != Bool;
}

inline ESavableBool operator!(const ESavableBool& SavableBool)
{
	return SavableBool == ESavableBool::ETrue ? ESavableBool::EFalse : ESavableBool::ETrue;
}
