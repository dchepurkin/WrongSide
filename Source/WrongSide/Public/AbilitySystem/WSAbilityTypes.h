// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "WSAbilityTypes.generated.h"

class UWSAbility;

USTRUCT(BlueprintType)
struct FWSAbilityInfo
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<UWSAbility> AbilityClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FGameplayTagContainer AbilityTags{};

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bIsInfinity = false;
};
