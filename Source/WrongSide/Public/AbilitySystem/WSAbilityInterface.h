// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemInterface.h"
#include "UObject/Interface.h"
#include "WSAbilityInterface.generated.h"

class UWSAbilitySystemComponent;
class UWSAttributeSet;

UINTERFACE(MinimalAPI, NotBlueprintable)
class UWSAbilityInterface : public UAbilitySystemInterface
{
	GENERATED_BODY()
};

class WRONGSIDE_API IWSAbilityInterface : public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	virtual UWSAbilitySystemComponent* GetAbilityComponent() const = 0;

	virtual UWSAttributeSet* GetAttributeSet() const = 0;

	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;
};
