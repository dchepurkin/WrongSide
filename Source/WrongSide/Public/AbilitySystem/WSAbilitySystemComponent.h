// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "WSAbilitySystemComponent.generated.h"

struct FWSAbilityInfo;
struct FInputActionValue;
class UWSAbilityData;

UCLASS()
class WRONGSIDE_API UWSAbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="WrongSide|Abilitiy")
	void ApplyEffectToSelf(TSubclassOf<UGameplayEffect> EffectClass, float Level);

	UFUNCTION(BlueprintCallable, Category="WrongSide|Abilitiy")
	void GiveAbilitiesFromData(UWSAbilityData* AbilityData);

	void SetActorType(const FGameplayTag& ActorTypeTag);

	void GiveAbilityFromInfo(const FWSAbilityInfo& AbilityInfo);

	void AbilityInputTagPressed(const FGameplayTag& InTag);

	void AbilityInputTagReleased(const FGameplayTag& InTag);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="WrongSide|Attributes|Startup")
	TSubclassOf<UGameplayEffect> StartupMaxVitalAttributes;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="WrongSide|Attributes|Startup")
	TSubclassOf<UGameplayEffect> StartupVitalAttributes;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="WrongSide|Attributes|Startup")
	TSubclassOf<UGameplayEffect> StartupPrimaryAttributes;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="WrongSide|Ability")
	TObjectPtr<UWSAbilityData> StartupAbilities;

	virtual void BeginPlay() override;

private:
	//TODO remove comments if need to remove infinity abilities
	/*UPROPERTY()
	TMap<FGameplayTag, FGameplayAbilitySpec> InfinityAbilities;*/

	void InitVitalAttributes();
	void InitPrimaryAttributes();

	void AbilityInputTag(const FGameplayTag& InTag, EAbilityGenericReplicatedEvent::Type EventType);
};
