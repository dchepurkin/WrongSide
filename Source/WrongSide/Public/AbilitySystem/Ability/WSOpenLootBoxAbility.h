// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Ability/WSAbility.h"
#include "WSOpenLootBoxAbility.generated.h"

class AWSLootBox;

UCLASS()
class WRONGSIDE_API UWSOpenLootBoxAbility : public UWSAbility
{
    GENERATED_BODY()

public:

protected:
    UPROPERTY(BlueprintReadOnly, Category="WrongSide|Ability")
    TObjectPtr<AWSLootBox> LootBox;

    virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
};
