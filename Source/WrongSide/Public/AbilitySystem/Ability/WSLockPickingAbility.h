// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Ability/WSAbility.h"
#include "WSLockPickingAbility.generated.h"

/**
 * 
 */
UCLASS()
class WRONGSIDE_API UWSLockPickingAbility : public UWSAbility
{
	GENERATED_BODY()
	
};
