// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Ability/WSAbility.h"
#include "WSPlayerJumpAbility.generated.h"

UCLASS()
class WRONGSIDE_API UWSPlayerJumpAbility : public UWSAbility
{
	GENERATED_BODY()

public:
	UWSPlayerJumpAbility();

protected:
	UPROPERTY()
	TObjectPtr<ACharacter> Character = nullptr;

	UFUNCTION(BlueprintNativeEvent, Category="Wrongide|Ability", DisplayName="OnLanded")
	void OnLanded_Callback(const FHitResult& HitResult);

	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

private:
	void OnLanded_Callback_Implementation(const FHitResult& HitResult);
};
