// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "WSMovePlayerToTask.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnMoveFinishedSignature);

UCLASS(meta=(ExposedAsyncProxy="AsyncTask"))
class WRONGSIDE_API UWSMovePlayerToTask : public UAbilityTask
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable)
    FOnMoveFinishedSignature Success;

    UPROPERTY(BlueprintAssignable)
    FOnMoveFinishedSignature Fail;

    UFUNCTION(BlueprintCallable, Category="WrongSide|Tasks", meta=(BlueprintInternalUseOnly=true, DefaultToSelf="OwningAbility", HidePin="OwningAbility"))
    static UWSMovePlayerToTask* MovePlayerTo(UGameplayAbility* OwningAbility, UPARAM(DisplayName="TargetLocation")FVector InTargetLocation, UPARAM(DisplayName="AcceptanceRadius")float InAcceptanceRadius = 120);

    UFUNCTION(BlueprintCallable, Category="WrongSide|Tasks")
    void FinishTask();

    virtual void TickTask(float DeltaTime) override;

protected:
    virtual void Activate() override;

private:
    UPROPERTY()
    TObjectPtr<AController> Controller = nullptr;

    UPROPERTY()
    TObjectPtr<APawn> Pawn = nullptr;

    FVector TargetLocation = FVector::ZeroVector;

    float AcceptanceRadius = 0.f;

    bool InTargetLocation();

    void OnOwnedAbilityEnded_Callback(UGameplayAbility* OwnedAbility);
};
