// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "WSWaitForCloseLootBoxTask.generated.h"

class UWSLootBoxWidget;
class AWSLootBox;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnLootBoxSignature);

UCLASS(meta=(ExposedAsyncProxy="AsyncTask"))
class WRONGSIDE_API UWSWaitForCloseLootBoxTask : public UAbilityTask
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable)
    FOnLootBoxSignature Closed;

    UFUNCTION(BlueprintCallable, Category="WrongSide|Tasks", meta=(BlueprintInternalUseOnly=true, DefaultToSelf="OwningAbility", HidePin="OwningAbility"))
    static UWSWaitForCloseLootBoxTask* WaitForCloseLootBox(UGameplayAbility* OwningAbility, UPARAM(DisplayName="LootBox")AWSLootBox* InLootBox);

    UFUNCTION(BlueprintCallable, Category="WrongSide|Tasks")
    void FinishTask();

private:
    UPROPERTY()
    TObjectPtr<AWSLootBox> LootBox = nullptr;

    UPROPERTY()
    TObjectPtr<UWSLootBoxWidget> LootBoxWidget = nullptr;

    UPROPERTY()
    TObjectPtr<APlayerController> Controller = nullptr;

    void Init(AWSLootBox* InLootBox);

    void OnOwnedAbilityEnded_Callback(UGameplayAbility* OwningAbility);
};
