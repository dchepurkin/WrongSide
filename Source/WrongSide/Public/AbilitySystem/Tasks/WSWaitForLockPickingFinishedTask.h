// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "WSWaitForLockPickingFinishedTask.generated.h"

UCLASS()
class WRONGSIDE_API UWSWaitForLockPickingFinishedTask : public UAbilityTask
{
	GENERATED_BODY()
	
};
