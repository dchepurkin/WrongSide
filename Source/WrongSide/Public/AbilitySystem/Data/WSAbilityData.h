// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/WSAbilityTypes.h"
#include "Engine/DataAsset.h"
#include "WSAbilityData.generated.h"

class UWSAbility;

UCLASS()
class WRONGSIDE_API UWSAbilityData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="WrongSide")
	TArray<FWSAbilityInfo> AbilityData;
};
