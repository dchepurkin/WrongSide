// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "AttributeSet.h"
#include "WSAttributeSet.generated.h"

#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

UCLASS()
class WRONGSIDE_API UWSAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, Category="WrongSide|Attributes|Vital")
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UWSAttributeSet, Health);

	UPROPERTY(BlueprintReadOnly, Category="WrongSide|Attributes|Vital")
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UWSAttributeSet, MaxHealth);
	
	UPROPERTY(BlueprintReadOnly, Category="WrongSide|Attributes|Vital")
	FGameplayAttributeData Mana;
	ATTRIBUTE_ACCESSORS(UWSAttributeSet, Mana);

	UPROPERTY(BlueprintReadOnly, Category="WrongSide|Attributes|Vital")
	FGameplayAttributeData MaxMana;
	ATTRIBUTE_ACCESSORS(UWSAttributeSet, MaxMana);

	UPROPERTY(BlueprintReadOnly, Category="WrongSide|Attributes|Vital")
	FGameplayAttributeData Stamina;
	ATTRIBUTE_ACCESSORS(UWSAttributeSet, Stamina);

	UPROPERTY(BlueprintReadOnly, Category="WrongSide|Attributes|Vital")
	FGameplayAttributeData MaxStamina;
	ATTRIBUTE_ACCESSORS(UWSAttributeSet, MaxStamina);

	UPROPERTY(BlueprintReadOnly, Category="WrongSide|Attributes|Primary")
	FGameplayAttributeData MaxWalkSpeed;
	ATTRIBUTE_ACCESSORS(UWSAttributeSet, MaxWalkSpeed);

	UPROPERTY(BlueprintReadOnly, Category="WrongSide|Attributes|Secondary")
	FGameplayAttributeData LockPickingLevel;
	ATTRIBUTE_ACCESSORS(UWSAttributeSet, LockPickingLevel);

	UWSAttributeSet();
};
