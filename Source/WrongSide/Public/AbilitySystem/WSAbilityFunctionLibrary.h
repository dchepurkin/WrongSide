// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "WSAbilityFunctionLibrary.generated.h"

class AWSLootBox;
class UWSAttributeSet;
class UWSAbilitySystemComponent;

UCLASS()
class WRONGSIDE_API UWSAbilityFunctionLibrary : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintPure, Category="WrongSide|Ability", meta=(DefaultToSelf="InActor"))
    static UWSAbilitySystemComponent* GetAbilityComponent(AActor* InActor);

    UFUNCTION(BlueprintPure, Category="WrongSide|Ability", meta=(DefaultToSelf="InActor"))
    static UWSAttributeSet* GetAttributeSet(AActor* InActor);

    UFUNCTION(BlueprintCallable, Category="WrongSide|Ability", meta=(DefaultToSelf="InActor"))
    static bool TryToActivateAbilityByTag(AActor* InActor, FGameplayTag InTag);

    UFUNCTION(BlueprintCallable, Category="WrongSide|Ability", meta=(DefaultToSelf="InActor"))
    static void TryToOpenLootBox(AActor* InActor,  AWSLootBox* InLootBox);

    UFUNCTION(BlueprintCallable, Category="WrongSide|Ability", meta=(DefaultToSelf="InActor"))
    static void SetAbilityInputTagPressed(AActor* InActor, FGameplayTag InTag);

    UFUNCTION(BlueprintCallable, Category="WrongSide|Ability", meta=(DefaultToSelf="InActor"))
    static void SetAbilityInputTagReleased(AActor* InActor, FGameplayTag InTag);
};
