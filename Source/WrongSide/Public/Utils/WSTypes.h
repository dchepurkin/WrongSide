// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "Inventory/WSInventoryTypes.h"
#include "Utils/WSConstants.h"
#include "WSTypes.generated.h"

USTRUCT(BlueprintType)
struct FWSInteractionActorInfo
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FText ActorName{};

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FText ActionName{};

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float MinFocusDistance = DefaultAcceptanceDistance;

    UPROPERTY(BlueprintReadWrite)
    FVector InteractionPosition = FVector::ZeroVector;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float AcceptanceRadius = DefaultAcceptanceDistance;
};

UENUM(BlueprintType)
enum class EWSLockType : uint8
{
    OpenWithPicklock,
    OpenWithSwitches,
    OpenWithItem
};

UENUM(BlueprintType)
enum class EWSLockDifficulty : uint8
{
    Easy,
    Normal,
    Hard,
    Impossible
};

USTRUCT(BlueprintType)
struct FWSMultiSwitchInfo
{
    GENERATED_BODY()

    //TODO Make switches info
};

USTRUCT(BlueprintType)
struct FWSLockInfo
{
    GENERATED_BODY()

    //��� �����������, �������� ��� ������(�������� ������ ���������)
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EWSLockType LockType = EWSLockType::OpenWithPicklock;

    //��� �������� ������������ �����
    UPROPERTY(EditAnywhere, BlueprintReadOnly,
        meta=(EditCondition="LockType != EWSLockType::OpenWithSwitches", EditConditionHides, ToolTip="The name of the item that opens the lock"))
    FWSInventoryItem OpeningItem;

    //��������� �����, �������� ������ ���� ������� ���������� � ������� �������
    UPROPERTY(EditAnywhere, BlueprintReadWrite,
        meta=(EditCondition="LockType == EWSLockType::OpenWithPicklock", EditConditionHides))
    EWSLockDifficulty LockDifficulty = EWSLockDifficulty::Easy;

    //������������� ��� ���������� �����
    UPROPERTY(EditAnywhere, BlueprintReadWrite,
        meta=(EditCondition="LockType == EWSLockType::OpenWithSwitches", EditConditionHides))
    FWSMultiSwitchInfo SwitchesToOpen;
};
