// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"

//~Begin Custom Collision Channels
constexpr ECollisionChannel ECC_IK = ECC_GameTraceChannel1;
constexpr ECollisionChannel ECC_Player = ECC_GameTraceChannel2;
constexpr ECollisionChannel ECC_Enemy = ECC_GameTraceChannel3;
constexpr ECollisionChannel ECC_Pickup = ECC_GameTraceChannel4;
constexpr ECollisionChannel ECC_Laser = ECC_GameTraceChannel5;

const FName PlayerCollisionProfile{"PlayerCollision"};
const FName PlayerMeshCollisionProfile{"PlayerMeshCollision"};
const FName EnemyCollisionProfile{"EnemyCollision"};
const FName EnemyMeshCollisionProfile{"EnemyMeshCollision"};
const FName PickupCollisionProfile{"PickupCollision"};
const FName PickupMeshCollisionProfile{"PickupMeshCollision"};
//~End Custom Collision Channels

//~Begin Custom Surface Type
constexpr EPhysicalSurface SurfaceType_BluntWood = SurfaceType1;
constexpr EPhysicalSurface SurfaceType_Concrete = SurfaceType2;
constexpr EPhysicalSurface SurfaceType_Grass = SurfaceType3;
constexpr EPhysicalSurface SurfaceType_Gravel = SurfaceType4;
constexpr EPhysicalSurface SurfaceType_Ground = SurfaceType5;
constexpr EPhysicalSurface SurfaceType_Marble = SurfaceType6;
constexpr EPhysicalSurface SurfaceType_Metal = SurfaceType7;
constexpr EPhysicalSurface SurfaceType_MetalBar = SurfaceType8;
constexpr EPhysicalSurface SurfaceType_Mud = SurfaceType9;
constexpr EPhysicalSurface SurfaceType_Sand = SurfaceType10;
constexpr EPhysicalSurface SurfaceType_Snow = SurfaceType11;
constexpr EPhysicalSurface SurfaceType_SqueakyWood = SurfaceType12;
constexpr EPhysicalSurface SurfaceType_Stone = SurfaceType13;
constexpr EPhysicalSurface SurfaceType_Water = SurfaceType14;
constexpr EPhysicalSurface SurfaceType_Wood = SurfaceType15;
//~End Custom Surface Type

constexpr float DefaultAcceptanceDistance = 45.f;

const FName DefaultWarpTargetName{"FacingTarget"};
