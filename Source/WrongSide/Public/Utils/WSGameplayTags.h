// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"

struct FWSTags
{
	FWSTags() = default;

	FWSTags(const FWSTags&) = delete;

	const FWSTags& operator=(const FWSTags&) = delete;

	static const FWSTags& Get() { return WSTags; }

	static void InitTags();

	//~Begin BodyParts Tags
	FGameplayTag BodyPart_Head;
	FGameplayTag BodyPart_Hair;
	FGameplayTag BodyPart_Eyebrows;
	FGameplayTag BodyPart_Tors;
	//~Ebd BodyParts Tags

	//~Begin Event Tags
	FGameplayTag Event;
	FGameplayTag Event_Interaction;
	FGameplayTag Event_Interaction_Started;
	FGameplayTag Event_Interaction_Finished;
	FGameplayTag Event_ScreenMessage;
	//~Ebd Event Tags

	//~Begin Player Tags
	FGameplayTag Player;
	FGameplayTag Player_Navigation;
	FGameplayTag Player_Navigation_MoveTo;
	FGameplayTag Player_Navigation_StopMoving;
	FGameplayTag Player_Navigation_MovingFinished;
	//~End Player Tags

	//~Begin Actor Tags
	FGameplayTag Actor;
	FGameplayTag Actor_Type;
	FGameplayTag Actor_Type_Item;
	FGameplayTag Actor_Type_None;
	FGameplayTag Actor_Type_Player;
	FGameplayTag Actor_Type_Enemy;
	FGameplayTag Actor_Type_Enemy_Aggressive;
	FGameplayTag Actor_Type_Enemy_Neutral;
	FGameplayTag Actor_Type_NPC;
	//~End Actor Tags

	//~Begin Attributes Tags
	FGameplayTag Attributes_Primary_Health;
	FGameplayTag Attributes_Primary_MaxHealth;
	FGameplayTag Attributes_Primary_Mana;
	FGameplayTag Attributes_Primary_MaxMana;
	FGameplayTag Attributes_Primary_Stamina;
	FGameplayTag Attributes_Primary_MaxStamina;
	//~End Attributes Tags

	//~Begin Input Tags
	FGameplayTag Input_Look;
	FGameplayTag Input_Jump;
	FGameplayTag Input_Move;
	FGameplayTag Input_Interact;
	FGameplayTag Input_Sprint;
	//~End Input Tags

	//~Begin Ability Tags
	FGameplayTag Ability;
	FGameplayTag Ability_Action;
	FGameplayTag Ability_Action_OpenLootBox;
	FGameplayTag Ability_Action_LockPicking;
	FGameplayTag Ability_Action_Sprint;
	FGameplayTag Ability_Action_Moving;
	FGameplayTag Ability_Action_Jump;
	FGameplayTag Ability_Action_Interaction;
	FGameplayTag Ability_Debuff_Infection;
	FGameplayTag Ability_Debuff_Infection_Poison;
	//~End Ability Tags

private:
	static FWSTags WSTags;

	void InitBodyPartsTags();
	void InitInputTags();
	void InitPlayerTags();
	void InitActorTags();
	void InitAttributesTags();
	void InitAbilityTags();
	void InitEventTags();
};
