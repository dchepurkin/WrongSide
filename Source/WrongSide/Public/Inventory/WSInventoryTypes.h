// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "WSInventoryTypes.generated.h"

USTRUCT(BlueprintType)
struct FWSInventoryItem
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(Categories="Item"))
    FGameplayTag ItemTag{};

    UPROPERTY(EditAnywhere, BlueprintReadOnly)
    int32 Amount = 1;
};
