// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "UI/Widgets/WSWidget.h"
#include "WSLootBoxWidget.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnLootBoxWidgetSignature)

UCLASS(Abstract)
class WRONGSIDE_API UWSLootBoxWidget : public UWSWidget
{
    GENERATED_BODY()

public:
    FOnLootBoxWidgetSignature OnClosed;

    UFUNCTION(BlueprintCallable, Category="WrongSide")
    void Close();
};
