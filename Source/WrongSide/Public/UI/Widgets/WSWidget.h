// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WSWidget.generated.h"

UCLASS(Abstract)
class WRONGSIDE_API UWSWidget : public UUserWidget
{
	GENERATED_BODY()
};
