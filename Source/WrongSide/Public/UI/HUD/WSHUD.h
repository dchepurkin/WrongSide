// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "WSHUD.generated.h"

class UWSWidget;
UCLASS(Abstract)
class WRONGSIDE_API AWSHUD : public AHUD
{
	GENERATED_BODY()

public:

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="WrongSide")
	TSubclassOf<UWSWidget> GameplayScreenClass;

	virtual void BeginPlay() override;

private:
	UPROPERTY()
	TObjectPtr<UWSWidget> GameplayScreenWidget = nullptr;
};
