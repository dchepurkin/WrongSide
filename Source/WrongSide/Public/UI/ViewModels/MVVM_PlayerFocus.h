// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "MVVMViewModelBase.h"
#include "MVVM_PlayerFocus.generated.h"

class UWSInteractionComponent;

UCLASS()
class WRONGSIDE_API UMVVM_PlayerFocus : public UMVVMViewModelBase
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, FieldNotify, Category="WrongSide|UI")
	bool bIsFocused = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, FieldNotify, Category="WrongSide|UI")
	TObjectPtr<AActor> FocusedActor = nullptr;

	UFUNCTION(BlueprintCallable, Category="WrongSide|UI")
	void InitBindings(UWSInteractionComponent* InteractionComponent);

	void SetIsFocused(bool IsFocused);

	void SetFocusedActor(AActor* InActor);

private:
	UFUNCTION()
	void OnActorFocused_Callback(AActor* InActor);

	UFUNCTION()
	void OnClearFocus_Callback();
};
