// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "MVVMViewModelBase.h"
#include "AbilitySystem/WSAttributeSet.h"
#include "MVVM_PrimaryAttributes.generated.h"

struct FOnAttributeChangeData;

UCLASS()
class WRONGSIDE_API UMVVM_PrimaryAttributes : public UMVVMViewModelBase
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="WrongSide|UI")
	void InitBindings(AActor* InActor);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, FieldNotify)
	float Health = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, FieldNotify)
	float MaxHealth = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, FieldNotify)
	float HealthPercent = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, FieldNotify)
	float Stamina = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, FieldNotify)
	float MaxStamina = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, FieldNotify)
	float StaminaPercent = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, FieldNotify)
	float Mana = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, FieldNotify)
	float MaxMana = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, FieldNotify)
	float ManaPercent = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, FieldNotify)
	bool bInfected = false;

	void SetHealth(const FOnAttributeChangeData& ValueData);
	void SetMaxHealth(const FOnAttributeChangeData& ValueData);
	void SetHealthPercent();

	void SetStamina(const FOnAttributeChangeData& ValueData);
	void SetMaxStamina(const FOnAttributeChangeData& ValueData);
	void SetStaminaPercent();

	void SetMana(const FOnAttributeChangeData& ValueData);
	void SetMaxMana(const FOnAttributeChangeData& ValueData);
	void SetManaPercent();

	void SetInfected(const FGameplayTag Tag, int32 Count);

private:
	void NotifyStartupValues(UWSAttributeSet* AttributeSet);
};
