// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Character/WSCharacterBase.h"
#include "Interfaces/WSPlayerInterface.h"
#include "SaveGame/SavablePlayerInterface.h"
#include "WSPlayerCharacter.generated.h"

class UWSInteractionComponent;
class UWSCameraComponent;
class UNavigationInvokerComponent;
class USpringArmComponent;

UCLASS(Abstract)
class WRONGSIDE_API AWSPlayerCharacter : public AWSCharacterBase, public ISavablePlayerInterface, public IWSPlayerInterface
{
	GENERATED_BODY()

public:
	AWSPlayerCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void PossessedBy(AController* NewController) override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TObjectPtr<USkeletalMeshComponent> HeadMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TObjectPtr<USkeletalMeshComponent> HairMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TObjectPtr<USkeletalMeshComponent> EyebrowsMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TObjectPtr<USpringArmComponent> SpringArm;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TObjectPtr<UWSCameraComponent> Camera;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TObjectPtr<UNavigationInvokerComponent> NavigationInvokerComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TObjectPtr<UWSInteractionComponent> InteractionComponent;

	UFUNCTION(BlueprintPure, Category="WrongSide|PlayerCharacter")
	USkeletalMeshComponent* GetBodyPartByTag(UPARAM(meta=(Categories="BodyPart"))FGameplayTag InTag) const;

	virtual void BeginPlay() override;

private:
	UPROPERTY()
	TMap<FGameplayTag, TObjectPtr<USkeletalMeshComponent>> BodyPartsMeshes;

	void OnCameraBeginOverlap_Callback() const;

	void OnCameraEndOverlap_Callback() const;

	USkeletalMeshComponent* CreateBodyPartMesh(const FName& InBodyPartName, const FGameplayTag& InTag);

	void SetBodyPartTag(USkeletalMeshComponent* InMesh, const FGameplayTag& InTag);

	//~Begin Player Interface
	virtual UWSInteractionComponent* GetInteractionComponent() const override { return InteractionComponent; };
	//~End Player Interface

	virtual void InitAbilityActorInfo() override;
};
