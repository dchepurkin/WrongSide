// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/WSAbilityInterface.h"
#include "GameFramework/Character.h"
#include "Interfaces/WSGameplayTagInterface.h"
#include "WSCharacterBase.generated.h"

class UWSAttributeSet;
class UWSAbilitySystemComponent;
class UMotionWarpingComponent;
struct FWSEventData;

UCLASS(Abstract)
class WRONGSIDE_API AWSCharacterBase : public ACharacter, public IWSGameplayTagInterface, public IWSAbilityInterface
{
    GENERATED_BODY()

public:
    AWSCharacterBase(const FObjectInitializer& ObjectInitializer);

    //~Begin Tag Interface
    virtual void GetOwnedGameplayTags(FGameplayTagContainer& InTagContainer) const override;

    virtual void SetActorType(FGameplayTag ActorTypeTag) override;
    //~End Tag Interface

    //~Begin Ability Interface
    virtual UWSAbilitySystemComponent* GetAbilityComponent() const override { return AbilityComponent; }

    virtual UWSAttributeSet* GetAttributeSet() const override { return AttributeSet; }
    //~End Ability Interface

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    TObjectPtr<UMotionWarpingComponent> MotionWarpingComponent;

    UPROPERTY()
    TObjectPtr<UWSAbilitySystemComponent> AbilityComponent = nullptr;

    UPROPERTY()
    TObjectPtr<UWSAttributeSet> AttributeSet = nullptr;

    virtual void BeginPlay() override;

    virtual void InitAbilityActorInfo() {};
};
