// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "WSAnimInstance.generated.h"

UCLASS()
class WRONGSIDE_API UWSAnimInstance : public UAnimInstance
{
    GENERATED_BODY()

public:
    virtual void NativeInitializeAnimation() override;

    virtual void NativeThreadSafeUpdateAnimation(float DeltaSeconds) override;

    void SetInteractionLoop(UAnimSequence* InLoop);

protected:
    UPROPERTY()
    TObjectPtr<APawn> PawnOwner = nullptr;

    UPROPERTY(BlueprintReadOnly, Category="WongSide|Animation")
    TObjectPtr<UAnimSequence> InteractionLoop = nullptr;

    UPROPERTY(BlueprintReadOnly, Category="WongSide|Animation")
    bool bIsInteracted = false;

    UPROPERTY(BlueprintReadOnly, Category="WongSide|Animation")
    float Speed = 0.f;

    UPROPERTY(BlueprintReadOnly, Category="WongSide|Animation")
    float Direction = 0.f;

    UPROPERTY(BlueprintReadOnly, Category="WongSide|Animation")
    bool bIsFalling = false;

    bool GetIsFalling() const;

};
