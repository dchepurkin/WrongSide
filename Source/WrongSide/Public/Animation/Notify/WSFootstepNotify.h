// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "WSFootstepNotify.generated.h"

UCLASS(Abstract, DisplayName="Footstep")
class WRONGSIDE_API UWSFootstepNotify : public UAnimNotify
{
	GENERATED_BODY()

public:
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference) override;

	virtual FString GetNotifyName_Implementation() const override { return FString{"Footstep"}; }

protected:
	UPROPERTY(EditInstanceOnly, Category="DungeonSeeker")
	FName BoneName{};

	UPROPERTY(EditAnywhere, Category="DungeonSeeker")
	float TraceLength = 100.f;

	UPROPERTY(EditInstanceOnly, Category="DungeonSeeker")
	float VolumeMultiplier = 1.f;

	UPROPERTY(EditDefaultsOnly, Category="DungeonSeeker")
	TMap<TEnumAsByte<EPhysicalSurface>, TObjectPtr<USoundBase>> SoundsMap;

private:
	bool BoneTrace(USkeletalMeshComponent* MeshComp, FHitResult& OutResult) const;
};
