// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "WSPlayerInterface.generated.h"

class UWSInteractionComponent;
UINTERFACE(MinimalAPI, BlueprintType, meta=(CannotImplementInterfaceInBlueprint))
class UWSPlayerInterface : public UInterface
{
	GENERATED_BODY()
};

class WRONGSIDE_API IWSPlayerInterface
{
	GENERATED_BODY()

public:
	virtual UWSInteractionComponent* GetInteractionComponent() const = 0;
};
