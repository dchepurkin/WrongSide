// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagAssetInterface.h"
#include "UObject/Interface.h"
#include "WSGameplayTagInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, BlueprintType, meta=(CannotImplementInterfaceInBlueprint))
class UWSGameplayTagInterface : public UGameplayTagAssetInterface
{
	GENERATED_BODY()
};

class WRONGSIDE_API IWSGameplayTagInterface : public IGameplayTagAssetInterface
{
	GENERATED_BODY()

public:
	virtual void SetActorType(UPARAM(meta=(Categories="Actor.Type"))FGameplayTag ActorTypeTag) = 0;
};
