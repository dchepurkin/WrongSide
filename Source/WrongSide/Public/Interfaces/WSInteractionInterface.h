// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Utils/WSTypes.h"
#include "WSInteractionInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, BlueprintType)
class UWSInteractionInterface : public UInterface
{
    GENERATED_BODY()
};

class WRONGSIDE_API IWSInteractionInterface
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WrongSide|Interaction")
    bool CanInteract(AActor* Interactor, FText& FailMessage);

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WrongSide|Interaction")
    bool CanFocus();

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WrongSide|Interaction")
    const FWSInteractionActorInfo GetInteractionActorInfo();

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WrongSide|Interaction")
    UAnimMontage* GetInteractionAnimation(AActor* Interactor);

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WrongSide|Interaction")
    UAnimSequence* GetInteractionLoopAnimation() const;

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WrongSide|Interaction")
    void Interact(AActor* Interactor);

private:
    virtual bool CanFocus_Implementation() { return true; }

    virtual bool CanInteract_Implementation(AActor* Interactor, FText& FailMessage) { return true; }

    virtual FWSInteractionActorInfo GetInteractionActorInfo_Implementation() { return {}; }

    virtual UAnimMontage* GetInteractionAnimation_Implementation(AActor* Interactor) const { return nullptr; };

    virtual UAnimSequence* GetInteractionLoopAnimation_Implementation() const { return nullptr; }

    virtual void Interact_Implementation(AActor* Interactor) {}
};
