// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/WSInteractionInterface.h"
#include "SaveGame/SavableActorInterface.h"
#include "WSLootBox.generated.h"

class UWSLootBoxWidget;
UCLASS(Abstract)
class WRONGSIDE_API AWSLootBox : public AActor, public IWSInteractionInterface, public ISavableActorInterface
{
    GENERATED_BODY()

public:
    AWSLootBox();

    UFUNCTION(BlueprintPure, Category="WrongSide")
    bool IsEmpty() const { return false; } //TODO check Inventory.IsEmpty

    UFUNCTION(BlueprintCallable, Category="WrongSide")
    virtual void Open(AActor* WhoOpen);

    const TSubclassOf<UWSLootBoxWidget>& GetLootBoxWidgetClass() const { return LootBoxWidgetClass; }

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="WrongSide")
    TObjectPtr<UStaticMeshComponent> Body;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="WrongSide")
    TObjectPtr<UStaticMeshComponent> Head;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="WrongSide|Interaction")
    FWSInteractionActorInfo InteractionInfo;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="WrongSide|Interaction")
    TObjectPtr<UAnimMontage> OpenAnimation;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="WrongSide|Interaction")
    TObjectPtr<UAnimSequence> OpenLoopAnimation;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="WrongSide|Interaction")
    TSubclassOf<UWSLootBoxWidget> LootBoxWidgetClass;

    virtual void BeginPlay() override;

private:
    //~Begin Interaction interface
    virtual bool CanFocus_Implementation() override { return !IsEmpty(); };

    virtual bool CanInteract_Implementation(AActor* Interactor, FText& FailMessage) override { return !IsEmpty(); };

    virtual FWSInteractionActorInfo GetInteractionActorInfo_Implementation() override;

    virtual UAnimMontage* GetInteractionAnimation_Implementation(AActor* Interactor) const override;

    virtual UAnimSequence* GetInteractionLoopAnimation_Implementation() const override { return OpenLoopAnimation; }

    virtual void Interact_Implementation(AActor* Interactor) override;
    //~End Interaction interface
};
