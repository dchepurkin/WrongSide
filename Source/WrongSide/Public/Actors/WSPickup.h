// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/WSInteractionInterface.h"
#include "SaveGame/SavableActorInterface.h"
#include "WSPickup.generated.h"

UCLASS(Abstract)
class WRONGSIDE_API AWSPickup : public AActor, public ISavableActorInterface, public IWSInteractionInterface
{
    GENERATED_BODY()

public:
    AWSPickup();

    void Take(AActor* WhoTake);

protected:
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="WrongSide|Interaction")
    FWSInteractionActorInfo InteractionInfo;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="WrongSide|Interaction")
    TObjectPtr<UAnimMontage> TakeDown;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="WrongSide|Interaction")
    TObjectPtr<UAnimMontage> TakeMiddle_100;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="WrongSide|Interaction")
    TObjectPtr<UAnimMontage> TakeMiddle_150;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="WrongSide|Interaction")
    TObjectPtr<UAnimMontage> TakeMiddle_200;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="WrongSide", AdvancedDisplay)
    float TakeDownHeightThreshold = -35.f;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="WrongSide", AdvancedDisplay)
    float TakeDownHeight_100_Threshold = 30.f;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="WrongSide", AdvancedDisplay)
    float TakeDownHeight_150_Threshold = 80.f;

    virtual void BeginPlay() override;

private:
    float CalculateHeight(AActor* InOtherActor) const;

    UAnimMontage* GetTakeMontageByHeight(float InHeight) const;

    //~Begin Interaction interface
    virtual bool CanFocus_Implementation() override { return true; };

    virtual bool CanInteract_Implementation(AActor* Interactor, FText& FailMessage) override { return true; };

    virtual FWSInteractionActorInfo GetInteractionActorInfo_Implementation() override;

    virtual UAnimMontage* GetInteractionAnimation_Implementation(AActor* Interactor) const override;

    virtual void Interact_Implementation(AActor* Interactor) override;
    //~End Interaction interface
};
