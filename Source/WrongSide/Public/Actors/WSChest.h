// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "Actors/WSLootBox.h"
#include "GameFramework/Actor.h"
#include "WSChest.generated.h"

class UWSLockComponent;

UCLASS(Abstract)
class WRONGSIDE_API AWSChest : public AWSLootBox
{
    GENERATED_BODY()

public:
    AWSChest();

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="WrongSide")
    TObjectPtr<UWSLockComponent> Lock;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="WrongSide")
    TObjectPtr<USceneComponent> InteractionPosition;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="WrongSide")
    bool bIsClosed = false;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="WrongSide", meta=(EditConditionHides, EditCondition="bIsClosed"))
    FWSLockInfo LockInfo;

    virtual void BeginPlay() override;

private:
    //~Begin Interaction interface
    virtual bool CanInteract_Implementation(AActor* Interactor, FText& FailMessage) override;

    virtual void Interact_Implementation(AActor* Interactor) override;
    //~End Interaction interface
};
