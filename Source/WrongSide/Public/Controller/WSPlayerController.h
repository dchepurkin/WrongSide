// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "WSPlayerController.generated.h"

struct FGameplayTag;
struct FInputActionValue;
class UInputMappingContext;
class UWSInputAction;

UCLASS(Abstract)
class WRONGSIDE_API AWSPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="WrongSide|Input")
	void SetInputMapping(UInputMappingContext* InMappingContext) const;

	virtual void SetupInputComponent() override;

protected:
	//~Begin Input Properties
	UPROPERTY(EditDefaultsOnly, Category="WrongSide|Input|Actions")
	TObjectPtr<UWSInputAction> LookAction;

	UPROPERTY(EditDefaultsOnly, Category="WrongSide|Input|Actions")
	TObjectPtr<UWSInputAction> MoveAction;

	UPROPERTY(EditDefaultsOnly, Category="WrongSide|Input|Actions")
	TObjectPtr<UWSInputAction> JumpAction;

	UPROPERTY(EditDefaultsOnly, Category="WrongSide|Input|Actions")
	TObjectPtr<UWSInputAction> InteractAction;

	UPROPERTY(EditDefaultsOnly, Category="WrongSide|Input|Actions")
	TObjectPtr<UWSInputAction> SprintAction;

	UPROPERTY(EditDefaultsOnly, Category="WrongSide|Input|Mappings")
	TObjectPtr<UInputMappingContext> DefaultMapping;
	//~End Input Properties

	UPROPERTY(EditDefaultsOnly, Category="WrongSide|Camera")
	float CameraFadeTime = 0.5f;

	UPROPERTY(EditDefaultsOnly, Category="WrongSide|Camera")
	float CameraFadeOutTime = 3.f;

	virtual void OnPossess(APawn* InPawn) override;

	virtual void BeginPlay() override;

private:
	FTimerHandle CameraFadeTimerHandle;

	void CameraFadeTimer_Callback();

	void OnLook_Callback(const FInputActionValue& Value);
	
	void OnMove_Callback(const FInputActionValue& Value);
	
	void TagInputReleased_Callback(const FGameplayTag InTag);

	void TagInputPressed_Callback(const FGameplayTag InTag);
};
