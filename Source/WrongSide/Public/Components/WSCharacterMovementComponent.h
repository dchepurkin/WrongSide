// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "WSCharacterMovementComponent.generated.h"

struct FOnAttributeChangeData;

UCLASS()
class WRONGSIDE_API UWSCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:
	UWSCharacterMovementComponent();

protected:
	virtual void BeginPlay() override;

private:
	void OnMaxWalkSpeedChanged_Callback(const FOnAttributeChangeData& WalkSpeedData);
};
