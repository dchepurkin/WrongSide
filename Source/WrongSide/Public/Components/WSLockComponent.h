// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SaveGame/SavableComponentInterface.h"
#include "Utils/WSTypes.h"
#include "WSLockComponent.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class WRONGSIDE_API UWSLockComponent : public UActorComponent, public ISavableComponentInterface
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, Category="WrongSide")
    void SetLockInfo(const bool IsClosed, UPARAM(DisplayName="LockInfo")const FWSLockInfo& InLockInfo);

    UFUNCTION(BlueprintPure, Category="WrongSide")
    bool CanOpen(AActor* WhoOpen, FText& OutFailMessage) const;

    UWSLockComponent();

protected:
    UPROPERTY(SaveGame)
    bool bIsClosed = false;

    FWSLockInfo LockInfo;

    virtual void BeginPlay() override;
};
