// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "WSCameraComponent.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnCameraOverlapSignature)

UCLASS()
class WRONGSIDE_API UWSCameraComponent : public UCameraComponent
{
	GENERATED_BODY()

public:
	FOnCameraOverlapSignature OnCameraBeginOverlap;

	FOnCameraOverlapSignature OnCameraEndOverlap;

	UWSCameraComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="WrongSide", meta=(ClampMin = 0.1f))
	float FadeDistance = 10.f;

private:
	bool bIsOverlapped = false;

	void CheckCameraCollision();

	
};
