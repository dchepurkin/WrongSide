// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "WSInteractionComponent.generated.h"

class AWSPlayerController;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FActorFocusedSignature, AActor*, InterctedActor);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnClearFocus);

UCLASS()
class WRONGSIDE_API UWSInteractionComponent : public USphereComponent
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="WrongSide|Interaction")
    FActorFocusedSignature OnActorFocused;

    UPROPERTY(BlueprintAssignable, Category="WrongSide|Interaction")
    FOnClearFocus OnClearFocus;

    UFUNCTION(BlueprintCallable, Category="WrongSide|Interaction")
    AActor* GetFocusedActor() const { return FocusedActor; }

    UFUNCTION(BlueprintCallable, Category="WrongSide|Interaction")
    void SetFocusActive(bool IsActive);

    UWSInteractionComponent();

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="WrongSide|Interaction")
    float TraceDistance = 5000.f;

    virtual void BeginPlay() override;

    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
    UPROPERTY()
    TObjectPtr<APawn> PawnOwner = nullptr;

    UPROPERTY()
    TObjectPtr<AWSPlayerController> Controller = nullptr;

    UPROPERTY()
    TObjectPtr<AActor> FocusedActor = nullptr;

    void ClearFocus();

    bool CanFocused(AActor* InActor) const;

    void SetActorFocused(AActor* InActor);

    bool CanPlayerSee(AActor* InActor) const;

    void GetCameraInfo(FVector& OutCameraLocation, FVector& OutCameraForwardVector) const;

    AActor* GetFirstOverlappingActor();
};
