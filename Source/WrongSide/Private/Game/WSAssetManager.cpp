// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Game/WSAssetManager.h"

#include "Utils/WSGameplayTags.h"

UWSAssetManager* UWSAssetManager::Get()
{
	check(GEngine);
	return Cast<UWSAssetManager>(GEngine->AssetManager);
}

void UWSAssetManager::StartInitialLoading()
{
	Super::StartInitialLoading();

	FWSTags::InitTags();
}
