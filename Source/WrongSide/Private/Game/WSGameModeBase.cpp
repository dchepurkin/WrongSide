// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Game/WSGameModeBase.h"

#include "Controller/WSPlayerController.h"

DEFINE_LOG_CATEGORY_STATIC(LogWSGameModeBase, All, All);

void AWSGameModeBase::MovePlayerToPlayerStart(const FString& PlayerStartTag)
{
	if(!GetWorld() || PlayerStartTag.IsEmpty()) return;

	const auto FindedPlayerStart = FindPlayerStart(nullptr, PlayerStartTag);
	if(!FindedPlayerStart) return;

	const auto PlayerController = GetWorld()->GetFirstPlayerController<AWSPlayerController>();
	if(!PlayerController) return;

	//PlayerController->TeleportPlayerPawn(FindedPlayerStart->GetActorTransform());
}

bool AWSGameModeBase::UpdatePlayerStartSpot(AController* Player, const FString& Portal, FString& OutErrorMessage)
{
	return Super::UpdatePlayerStartSpot(Player, StartSpotName, OutErrorMessage);
}
