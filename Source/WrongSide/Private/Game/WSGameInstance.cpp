// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Game/WSGameInstance.h"

#include "SaveGame/SaveGameManager.h"
#include "Game/WSGameModeBase.h"

DEFINE_LOG_CATEGORY_STATIC(LogWSGameInstance, All, All);

void UWSGameInstance::Init()
{
	Super::Init();	
}

void UWSGameInstance::StartNewGame()
{
	if(const auto SaveGameManager = USaveGameManager::GetSaveGameManager())
	{
		SaveGameManager->RemoveAutoSave();
	}
	MovePlayerTo("", NewGameMapName);
}

void UWSGameInstance::LoadGameFromSlot(const FString& InSlotName)
{
	//TODO Remove this from GameInstance
	/*LoadingSlotName = InSlotName;
	if(const auto SaveGameManager = USaveGameManager::GetSaveGameManager())
	{
		const auto LoadingLevelName = SaveGameManager->GetLevelNameInSlot(LoadingSlotName);
		MovePlayerTo("", LoadingLevelName);
	}*/
}

UTexture2D* UWSGameInstance::GetMapLoadingImage(const FName& MapName) const
{
	/*if(!MapsInfoDataTable) return nullptr;

	const auto FindedRow = MapsInfoDataTable->FindRow<FWSMapInfo>(MapName, "");
	return FindedRow ? FindedRow->LoadingScreenImage : nullptr;*/
	return nullptr;
}

void UWSGameInstance::MovePlayerTo(const FString& PlayerStartTag, const FName LevelName)
{
	CurrentPlayerStartTag = PlayerStartTag;

	const auto CurrentLevelName = UGameplayStatics::GetCurrentLevelName(this);
	if(LevelName.IsNone() || (!PlayerStartTag.IsEmpty() && LevelName.ToString() == CurrentLevelName))
	{
		MovePlayerToPlayerStart();
	}
	else
	{
		//TODO GameInstance �� ������ �������� �� HUD
		/*GetHUD()->ShowLoadingScreen(GetMapLoadingImage(LevelName));
		GetHUD()->OnLoadingScreenFinished.AddLambda([this, LevelName]() { UGameplayStatics::OpenLevel(this, LevelName); });*/
	}
}


//TODO Move player after map is loaded
/*void UWSGameInstance::PostLoadMap_Callback(UWorld* LoadedWorld)
{
	if(const auto SaveGameManager = USaveGameManager::GetSaveGameManager())
	{
		
	}

	MovePlayerToPlayerStart();
}*/

void UWSGameInstance::MovePlayerToPlayerStart() const
{
	if(!GetWorld()) return;

	if(const auto WSGameMode = GetWorld()->GetAuthGameMode<AWSGameModeBase>())
	{
		WSGameMode->MovePlayerToPlayerStart(CurrentPlayerStartTag);
	}
}
