// Created by Dmitriy Chepurkin. All rights reserved

#include "SaveGame/SaveGameManager.h"

#include "EngineUtils.h"
#include "SaveGame/SavableActorInterface.h"
#include "SaveGame/SavablePlayerInterface.h"
#include "SaveGame/SaveGameData.h"
#include "SaveGame/ScreenshotMaker.h"
#include "SaveGame/SaveGameInfo.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Serialization/ObjectAndNameAsStringProxyArchive.h"

DEFINE_LOG_CATEGORY_STATIC(LogSaveGameManager, All, All);

void USaveGameManager::Initialize(FSubsystemCollectionBase& Collection)
{
    Super::Initialize(Collection);

    check(GetWorld());

    SaveGameManager = this;
    FInternationalization::Get().SetCurrentLocale("RU");

    FCoreUObjectDelegates::PreLoadMap.AddUObject(this, &ThisClass::PreLoadMap_Callback);
    FCoreUObjectDelegates::PostLoadMapWithWorld.AddUObject(this, &ThisClass::PostLoadMap_Callback);
}

void USaveGameManager::PreLoadMap_Callback(const FString& LoadedLevelName)
{
    UE_LOG(LogSaveGameManager, Warning, TEXT("Pre load map"));
    CreateAutoSave();
}

void USaveGameManager::PostLoadMap_Callback(UWorld* LoadedWorld)
{
    UE_LOG(LogSaveGameManager, Warning, TEXT("Map is loaded"));

    LoadingSlotName.IsEmpty()
        ? SaveGameManager->LoadAutoSave()
        : SaveGameManager->LoadGameFromSlot(LoadingSlotName);

    LoadingSlotName.Empty();
}

bool USaveGameManager::SaveGameToSlot(const FString& InSlotName)
{
    if(InSlotName.IsEmpty()) return false;

    if(IsAlreadySavedInSlot(InSlotName))
    {
        RemoveSaveGameInSlot(InSlotName);
    }

    //Save Game Info
    const auto SaveGameSlotsInfo = GetSaveGameSlotsInfo();
    if(!SaveGameSlotsInfo) return false;

    TArray<FColor> ScreenshotData;
    CreateScreenshot(ScreenshotData);
    if(!SaveGameSlotsInfo->Save(InSlotName, ScreenshotData)) return false;
    //End of saving game info

    //Save Game Data
    CreateAutoSave();
    const auto SaveGameSlot = GetSaveGameSlot(InSlotName);
    if(!SaveGameSlot) return false;

    if(!SaveGameSlot->Save(InSlotName, GetCurrentLevelName(), PlayerSaveData, WorldSaveData))
    {
        RemoveSaveGameInSlot(InSlotName);
        return false;
    }
    //End of Saving Data

    OnGameSaved.Broadcast();

    UE_LOG(LogSaveGameManager, Display, TEXT("Game saved to slot %s"), *InSlotName);
    return true;
}

void USaveGameManager::RemoveSaveGameInSlot(const FString& InSlotName)
{
    if(const auto SaveGameSlotsInfo = GetSaveGameSlotsInfo())
    {
        SaveGameSlotsInfo->Remove(InSlotName);
        UE_LOG(LogSaveGameManager, Display, TEXT("Save game removed in slot %s"), *InSlotName);
    }
}

void USaveGameManager::LoadGameFromSlot(const FString& InSlotName)
{
    const auto SaveGameSlot = GetSaveGameSlot(InSlotName);
    if(!SaveGameSlot) return;

    PlayerSaveData = SaveGameSlot->GetPlayerSaveData();
    WorldSaveData = SaveGameSlot->GetWorldSaveData();

    LoadSaveData(false);
    OnGameLoaded.Broadcast();

    UE_LOG(LogSaveGameManager, Display, TEXT("Game loaded from slot %s"), *InSlotName);
}

void USaveGameManager::RemoveAutoSave()
{
    PlayerSaveData = FPlayerSaveData{};
    WorldSaveData.Empty();
}

void USaveGameManager::GetSlots(TArray<FSaveInfo>& Slots)
{
    if(const auto SaveGameSlotsInfo = GetSaveGameSlotsInfo())
    {
        SaveGameSlotsInfo->GetSlotsInfo(Slots);
    }
}

bool USaveGameManager::HaveAnySavedGames()
{
    TArray<FSaveInfo> Slots;
    GetSlots(Slots);

    return !Slots.IsEmpty();
}

UTexture2D* USaveGameManager::GetScreenshot(const FSaveInfo& SlotInfo)
{
    TArray<uint8> Screenshot = SlotInfo.Screenshot;
    if(Screenshot.IsEmpty()) return nullptr;

    UTexture2D* Texture = UTexture2D::CreateTransient(ScreenshotResolution.X, ScreenshotResolution.Y);
    FTexture2DMipMap& Mip = Texture->GetPlatformData()->Mips[0];

    const int32 BufferSize = Screenshot.Num();

    void* MipBulkData = Mip.BulkData.Lock(LOCK_READ_WRITE);
    Mip.BulkData.Realloc(BufferSize);
    FMemory::Memcpy(MipBulkData, Screenshot.GetData(), BufferSize);
    Mip.BulkData.Unlock();
    Texture->UpdateResource();

    return Texture;
}

bool USaveGameManager::IsAlreadySavedInSlot(const FString& InSlotName)
{
    return UGameplayStatics::DoesSaveGameExist(InSlotName, 0);
}

void USaveGameManager::CreateAutoSave()
{
    FLevelSaveData LevelSaveData;
    PlayerSaveData = FPlayerSaveData{};
    GetSaveData(LevelSaveData, PlayerSaveData);

    WorldSaveData.Add(GetCurrentLevelName(), LevelSaveData);
    UE_LOG(LogSaveGameManager, Display, TEXT("Auto Save Created"));
}

void USaveGameManager::LoadAutoSave()
{
    LoadSaveData(true);
    UE_LOG(LogSaveGameManager, Display, TEXT("Game loaded from Auto save"));
}

void USaveGameManager::GetSaveData(FLevelSaveData& OutLevelData, FPlayerSaveData& OutPlayerData)
{
    //Итерируемся по всем акторам, акторы с интерфейсом USavableActor сохраняем в OutSaveData
    for(FActorIterator It(GetWorld()); It; ++It)
    {
        const auto Actor = *It;
        if(!IsValid(Actor)) continue;

        if(Actor->Implements<USavableActorInterface>())
        {
            FActorSaveData ActorSaveData;
            GetActorData(Actor, ActorSaveData);
            OutLevelData.LevelData.Add(ActorSaveData);
        }
        else if(Actor->Implements<USavablePlayerInterface>())
        {
            GetPlayerData(Actor, OutPlayerData);
        }
    }
}

void USaveGameManager::LoadSaveData(const bool IsAutoSaveData)
{
    LoadedObjects.Empty(LoadedObjects.Num());

    TArray<AActor*> SavableActors;
    AActor* Player = nullptr;

    for(FActorIterator It(GetWorld()); It; ++It)
    {
        const auto Actor = *It;
        if(!IsValid(Actor)) continue;

        if(Actor->Implements<USavableActorInterface>())
        {
            SavableActors.Add(Actor);
        }
        else if(Actor->Implements<USavablePlayerInterface>())
        {
            Player = Actor;
        }
    }

    //Загружаем состояние всех акторов
    if(const auto LevelName = GetCurrentLevelName(); WorldSaveData.Contains(LevelName))
    {
        const auto LevelData = WorldSaveData[LevelName].LevelData;
        for(const auto& SaveData : LevelData)
        {
            auto FoundActor = SavableActors.FindByPredicate([&SaveData](const AActor* InActor)
            {
                return InActor->GetFName() == SaveData.Name;
            });

            AActor* LoadedActor = nullptr;
            if(FoundActor) //если найден актор на сцене то удаляем его из массива
            {
                LoadedActor = *FoundActor;
                SavableActors.RemoveSingle(*FoundActor);
            }
            else //Если в сохранениях есть актор которого нет на сцене то спавним его
            {
                LoadedActor = GetWorld()->SpawnActor(SaveData.Class);
            }

            LoadActorData(LoadedActor, SaveData);
        }

        //если после загрузки в массиве остались акторы то удаляем их
        for(const auto Actor : SavableActors)
        {
            Actor->Destroy();
        }
    }

    //Загружаем состояние игрока
    LoadPlayerData(Player, PlayerSaveData, IsAutoSaveData);

    for(const auto Object : LoadedObjects)
    {
        //Возможно разыменование нулевого указателя
        if(!Object || !Object->Implements<USavableObjectInterface>()) continue;
        ISavableObjectInterface::Execute_OnGameLoaded(Object);
    }
}

void USaveGameManager::GetActorData(AActor* InActor, FActorSaveData& OutData)
{
    GetByteData(InActor, OutData);
    GetComponentsData(InActor, OutData);

    OutData.Class = InActor->GetClass();
    OutData.Name = InActor->GetFName();
    OutData.Transform = InActor->GetActorTransform();
}

void USaveGameManager::LoadActorData(AActor* InActor, const FActorSaveData& InData)
{
    if(!InActor) return;

    LoadByteData(InActor, InData);
    LoadComponentsData(InActor, InData);
    InActor->SetActorTransform(InData.Transform);

    LoadedObjects.Add(InActor);
    ISavableActorInterface::Execute_OnActorLoaded(InActor);
}

void USaveGameManager::GetPlayerData(AActor* InPlayerActor, FPlayerSaveData& OutData)
{
    GetByteData(InPlayerActor, OutData);
    GetComponentsData(InPlayerActor, OutData);

    OutData.Transform = InPlayerActor->GetActorTransform();

    if(const auto Pawn = Cast<APawn>(InPlayerActor))
    {
        OutData.ControlRotation = Pawn->GetControlRotation();

        if(const auto MovementComponent = Pawn->FindComponentByClass<UCharacterMovementComponent>())
        {
            OutData.MovementMode = MovementComponent->MovementMode;
        }
    }
}

void USaveGameManager::LoadPlayerData(AActor* InPlayerActor, const FPlayerSaveData& InData, bool IsAutoSaveData)
{
    if(!InPlayerActor) return;

    LoadByteData(InPlayerActor, InData);
    LoadComponentsData(InPlayerActor, InData);

    if(!IsAutoSaveData)
    {
        InPlayerActor->SetActorTransform(InData.Transform);

        if(const auto Pawn = Cast<APawn>(InPlayerActor))
        {
            if(const auto Controller = Pawn->GetController())
            {
                Controller->SetControlRotation(InData.ControlRotation);
            }

            if(const auto MovementComponent = Pawn->FindComponentByClass<UCharacterMovementComponent>())
            {
                MovementComponent->SetMovementMode(InData.MovementMode);
            }
        }
    }

    LoadedObjects.Add(InPlayerActor);
    ISavablePlayerInterface::Execute_OnPlayerLoaded(InPlayerActor);
}

void USaveGameManager::GetByteData(UObject* InObject, FSaveData& OutData) const
{
    FMemoryWriter MemoryWriter(OutData.ByteData);
    FObjectAndNameAsStringProxyArchive Archive(MemoryWriter, true);
    Archive.ArIsSaveGame = true;

    InObject->Serialize(Archive);
}

void USaveGameManager::LoadByteData(UObject* InObject, const FSaveData& InData) const
{
    FMemoryReader MemoryReader(InData.ByteData);
    FObjectAndNameAsStringProxyArchive Archive(MemoryReader, true);
    Archive.ArIsSaveGame = true;

    InObject->Serialize(Archive);
}

void USaveGameManager::CreateScreenshot(TArray<FColor>& OutScreenshot) const
{
    if(!GetWorld()) return;

    const auto ScreenshotMaker = GetWorld()->SpawnActor<AScreenshotMaker>();
    if(!ScreenshotMaker) return;

    ScreenshotMaker->CreateScreenshot(ScreenshotResolution, OutScreenshot);
    ScreenshotMaker->Destroy();
}

FName USaveGameManager::GetLevelNameInSlot(const FString& InSlotName) const
{
    FName LevelName;
    if(const auto SaveGameSlot = GetSaveGameSlot(InSlotName))
    {
        LevelName = SaveGameSlot->GetLevelName();
    }

    return LevelName;
}

USaveGameInfo* USaveGameManager::GetSaveGameSlotsInfo()
{
    if(!SaveGameInfoCache)
    {
        SaveGameInfoCache = GetSaveGameObject<USaveGameInfo>("SlotsInfo");
    }
    return SaveGameInfoCache;
}

USaveGameData* USaveGameManager::GetSaveGameSlot(const FString& InSlotName) const
{
    return GetSaveGameObject<USaveGameData>(InSlotName);
}

FName USaveGameManager::GetCurrentLevelName() const
{
    return FName(UGameplayStatics::GetCurrentLevelName(this));
}
