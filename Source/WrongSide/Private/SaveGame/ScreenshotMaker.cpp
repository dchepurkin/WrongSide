// Created by Dmitriy Chepurkin. All rights reserved

#include "SaveGame/ScreenshotMaker.h"

#include "Components/SceneCaptureComponent2D.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Kismet/GameplayStatics.h"

AScreenshotMaker::AScreenshotMaker()
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	Camera = CreateDefaultSubobject<USceneCaptureComponent2D>("Camera");
	SetRootComponent(Camera);

	Camera->PostProcessBlendWeight = 1.f;
	Camera->bCaptureEveryFrame = false;
	Camera->CaptureSource = SCS_FinalColorHDR;
}

bool AScreenshotMaker::CreateScreenshot(const FVector2D& Resolution, TArray<FColor>& ColorData)
{
	if(!SetCameraToPlayerView()) return false;

	const auto TextureRenderTarget = NewObject<UTextureRenderTarget2D>();
	TextureRenderTarget->InitCustomFormat(Resolution.X, Resolution.Y, PF_B8G8R8A8, false);

	Camera->TextureTarget = TextureRenderTarget;
	Camera->CaptureScene();

	ColorData.Empty();
	ColorData.Reserve(Resolution.X * Resolution.Y);
	TextureRenderTarget->GameThread_GetRenderTargetResource()->ReadPixels(ColorData);
	ColorData.Shrink();

	TextureRenderTarget->MarkAsGarbage();

	return true;
}

bool AScreenshotMaker::SetCameraToPlayerView()
{
	if(!Camera) return false;

	const auto PlayerCamera = UGameplayStatics::GetPlayerCameraManager(this, 0);
	if(!PlayerCamera) return false;

	const auto CameraLocation = PlayerCamera->GetCameraLocation();
	const auto CameraRotation = PlayerCamera->GetCameraRotation();

	SetActorLocationAndRotation(CameraLocation, CameraRotation);
	Camera->SetWorldLocationAndRotation(CameraLocation, CameraRotation);

	Camera->FOVAngle = PlayerCamera->GetFOVAngle();
	return true;
}
