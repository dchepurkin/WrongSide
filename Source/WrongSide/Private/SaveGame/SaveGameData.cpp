// Created by Dmitriy Chepurkin. All rights reserved

#include "SaveGame/SaveGameData.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogSaveGameData, All, All);

bool USaveGameData::Save(const FString& InSlotName, const FName& InLevelName, const FPlayerSaveData& InPlayerSaveData,
                         const TMap<FName, FLevelSaveData>& InWorldSaveData)
{
	LevelName = InLevelName;
	PlayerSaveData = InPlayerSaveData;
	WorldSaveData = InWorldSaveData;

	return UGameplayStatics::SaveGameToSlot(this, InSlotName, 0);
}
