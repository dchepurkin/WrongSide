// Created by Dmitriy Chepurkin. All rights reserved

#include "SaveGame/SaveGameInfo.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogSaveGameSlot, All, All);

bool USaveGameInfo::Save(const FString& InSlotName, const TArray<FColor>& Screenshot)
{
	//May be try to use UGameplayStatics::AsyncSaveGameToSlot()
	SaveGamesInfo.EmplaceAt(0, FSaveInfo(InSlotName, Screenshot));
	return UGameplayStatics::SaveGameToSlot(this, "SlotsInfo", 0);
}

bool USaveGameInfo::Remove(const FString& InSlotName)
{
	const auto FindedSlotIndex = SaveGamesInfo.IndexOfByPredicate([&](const FSaveInfo& Slot)
	{
		return Slot.SlotName == InSlotName;
	});

	if(FindedSlotIndex == INDEX_NONE) return false;

	SaveGamesInfo.RemoveAt(FindedSlotIndex);
	UGameplayStatics::DeleteGameInSlot(InSlotName, 0);
	return UGameplayStatics::SaveGameToSlot(this, "SlotsInfo", 0);
}
