// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Components/WSCharacterMovementComponent.h"

#include "AbilitySystem/WSAbilityFunctionLibrary.h"
#include "AbilitySystem/WSAbilitySystemComponent.h"
#include "AbilitySystem/WSAttributeSet.h"

DEFINE_LOG_CATEGORY_STATIC(LogWSCharacterMovementComponent, All, All);

UWSCharacterMovementComponent::UWSCharacterMovementComponent()
{
	PrimaryComponentTick.bStartWithTickEnabled = false;

	bUseControllerDesiredRotation = true;
	bOrientRotationToMovement = true;
	RotationRate = FRotator(0.f, 350.f, 0.f);
	JumpZVelocity = 450.f;
	GravityScale = 1.5f;
	MaxAcceleration = 1024.f;
}

void UWSCharacterMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	if(const auto AbilityComponent = UWSAbilityFunctionLibrary::GetAbilityComponent(GetOwner()))
	{
		if(const auto AttributeSet = UWSAbilityFunctionLibrary::GetAttributeSet(GetOwner()))
		{
			AbilityComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMaxWalkSpeedAttribute()).AddUObject(this, &ThisClass::OnMaxWalkSpeedChanged_Callback);
			MaxWalkSpeed = AttributeSet->GetMaxWalkSpeed(); //Set walk Speed on first run
		}
	}
}

void UWSCharacterMovementComponent::OnMaxWalkSpeedChanged_Callback(const FOnAttributeChangeData& WalkSpeedData)
{
	MaxWalkSpeed = WalkSpeedData.NewValue;
}
