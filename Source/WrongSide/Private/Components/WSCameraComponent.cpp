// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Components/WSCameraComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogWSCameraComponent, All, All);

UWSCameraComponent::UWSCameraComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;
	PrimaryComponentTick.TickInterval = 0.1f;
}

void UWSCameraComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	CheckCameraCollision();
}

void UWSCameraComponent::CheckCameraCollision()
{
	if(!GetWorld()) return;

	const auto StartPoint = GetComponentLocation();
	const auto Direction = GetForwardVector() * FadeDistance;
	const auto EndPoint = StartPoint + Direction;

	FHitResult HitResult;

	FCollisionObjectQueryParams CollisionObjectQueryParams;
	CollisionObjectQueryParams.AddObjectTypesToQuery(GetOwner()->GetRootComponent()->GetCollisionObjectType());

	GetWorld()->LineTraceSingleByObjectType(HitResult, StartPoint, EndPoint, CollisionObjectQueryParams);

	if(!HitResult.bBlockingHit && bIsOverlapped)
	{
		bIsOverlapped = false;
		OnCameraEndOverlap.Broadcast();
	}
	else if(HitResult.bBlockingHit && !bIsOverlapped && HitResult.GetActor() == GetOwner())
	{
		bIsOverlapped = true;
		OnCameraBeginOverlap.Broadcast();
	}
}


