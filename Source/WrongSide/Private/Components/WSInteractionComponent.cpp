// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Components/WSInteractionComponent.h"
#include "Interfaces/WSInteractionInterface.h"
#include "Controller/WSPlayerController.h"
#include "FunctionLibraries/WSFunctionLibrary.h"
#include "Utils/WSConstants.h"
#include "Utils/WSTypes.h"

DEFINE_LOG_CATEGORY_STATIC(LogWSInteractionComponent, All, All);

UWSInteractionComponent::UWSInteractionComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
    PrimaryComponentTick.TickInterval = 0.1f;
}

void UWSInteractionComponent::BeginPlay()
{
    Super::BeginPlay();

    PawnOwner = GetOwner<APawn>();
    check(PawnOwner);
    Controller = PawnOwner->GetController<AWSPlayerController>();
    SetCollisionResponseToChannel(ECC_Player, ECR_Ignore);
}

void UWSInteractionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    if(const auto World = GetWorld())
    {
        FVector StartTrace;
        FVector CameraForwardVector;
        GetCameraInfo(StartTrace, CameraForwardVector);
        const auto EndTrace = StartTrace + CameraForwardVector * TraceDistance;

        FHitResult HitResult;
        FCollisionQueryParams QueryParams;
        QueryParams.AddIgnoredActor(PawnOwner);

        GetWorld()->LineTraceSingleByChannel(HitResult, StartTrace, EndTrace, ECC_Visibility, QueryParams);

        auto NewFocusedActor = HitResult.GetActor();

        //TODO � ��� ����� ����������� ������ Trace (��� �����)
        NewFocusedActor = CanPlayerSee(NewFocusedActor) ? NewFocusedActor : GetFirstOverlappingActor();

        if(NewFocusedActor)
        {
            SetActorFocused(NewFocusedActor);
        }
        else
        {
            ClearFocus();
        }
    }
}

AActor* UWSInteractionComponent::GetFirstOverlappingActor()
{
    UpdateOverlaps(nullptr, false);

    TArray<AActor*> OverlappingActors;
    GetOverlappingActors(OverlappingActors);

    for(const auto Actor : OverlappingActors)
    {
        if(CanPlayerSee(Actor))
        {
            return Actor;
        }
    }

    return nullptr;
}

bool UWSInteractionComponent::CanPlayerSee(AActor* InActor) const
{
    if(!IsValid(InActor) || !InActor->Implements<UWSInteractionInterface>() || !IWSInteractionInterface::Execute_CanFocus(InActor)) { return false; }

    const auto MinDistance = IWSInteractionInterface::Execute_GetInteractionActorInfo(InActor).MinFocusDistance;
    if(FVector::Distance(PawnOwner->GetActorLocation(), InActor->GetActorLocation()) >= MinDistance) { return false; }

    const auto EndTrace = InActor->GetActorLocation();

    FCollisionQueryParams QueryParams;
    QueryParams.AddIgnoredActor(PawnOwner);

    FHitResult HitResult;
    GetWorld()->LineTraceSingleByChannel(HitResult, PawnOwner->GetPawnViewLocation(), EndTrace, ECC_Visibility, QueryParams);

    return HitResult.bBlockingHit && HitResult.GetActor() == InActor;
}

void UWSInteractionComponent::ClearFocus()
{
    UWSFunctionLibrary::SetOutlineEnabled(FocusedActor, false);

    if(FocusedActor)
    {
        FocusedActor = nullptr;
        OnClearFocus.Broadcast();
    }
}

void UWSInteractionComponent::SetActorFocused(AActor* InActor)
{
    if(InActor == FocusedActor && CanFocused(FocusedActor)) return;

    ClearFocus();

    if(CanFocused(InActor))
    {
        FocusedActor = InActor;
        UWSFunctionLibrary::SetOutlineEnabled(FocusedActor, true);

        OnActorFocused.Broadcast(FocusedActor);
    }
}

bool UWSInteractionComponent::CanFocused(AActor* InActor) const
{
    return IsValid(InActor) && InActor->Implements<UWSInteractionInterface>() ? IWSInteractionInterface::Execute_CanFocus(InActor) : false;
}

void UWSInteractionComponent::GetCameraInfo(FVector& OutCameraLocation, FVector& OutCameraForwardVector) const
{
    FRotator CameraRotation;
    Controller->GetPlayerViewPoint(OutCameraLocation, CameraRotation);
    OutCameraForwardVector = CameraRotation.Vector();
}

void UWSInteractionComponent::SetFocusActive(bool IsActive)
{
    if(!IsActive) ClearFocus();
    SetComponentTickEnabled(IsActive);
}
