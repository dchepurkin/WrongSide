// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Components/WSLockComponent.h"

UWSLockComponent::UWSLockComponent()
{
    PrimaryComponentTick.bStartWithTickEnabled = false;
    PrimaryComponentTick.bCanEverTick = false;
}

void UWSLockComponent::BeginPlay()
{
    Super::BeginPlay();
}

void UWSLockComponent::SetLockInfo(const bool IsClosed, const FWSLockInfo& InLockInfo)
{
    LockInfo = InLockInfo;
    bIsClosed = IsClosed;

    //TODO �������������� ����� �������� ������
    /*if(LockInfo.LockType == EWSLockType::OpenWithSwitches)
    {
        LockInfo.SwitchesToOpen.BindSwitches(this, "OnSwitch_Callback");
        bIsClosed = IsClosed ? !LockInfo.SwitchesToOpen.IsSwitchesInRightCondition() : false;
    }
    else
    {
        bIsClosed = IsClosed;
    }*/
}

bool UWSLockComponent::CanOpen(AActor* WhoOpen, FText& OutFailMessage) const
{
    return true;
}
