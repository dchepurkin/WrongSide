// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Animation/Notify/WSFootstepNotify.h"
#include "Kismet/GameplayStatics.h"
#include "Utils/WSConstants.h"

DEFINE_LOG_CATEGORY_STATIC(LogWSFootstepNotify, All, All);

void UWSFootstepNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
	Super::Notify(MeshComp, Animation, EventReference);

	if(FHitResult HitResult; BoneTrace(MeshComp, HitResult))
	{
		if(const auto PhysMat = HitResult.PhysMaterial; PhysMat.IsValid())
		{
			const auto SurfaceType = HitResult.PhysMaterial->SurfaceType;
			USoundBase* Sound = SoundsMap.Contains(SurfaceType) ? SoundsMap[SurfaceType] : nullptr;
			
			UGameplayStatics::SpawnSoundAttached(Sound, MeshComp, BoneName, FVector::ZeroVector, EAttachLocation::KeepRelativeOffset, false, VolumeMultiplier);
		}
	}
}

bool UWSFootstepNotify::BoneTrace(USkeletalMeshComponent* MeshComp, FHitResult& OutResult) const
{
	if(!MeshComp) { return false; }

	if(const auto World = MeshComp->GetWorld())
	{
		const auto BoneLocation = MeshComp->GetBoneLocation(BoneName);
		const auto EndTrace = BoneLocation - FVector{0.f, 0.f, TraceLength};
		FCollisionQueryParams QueryParams;
		QueryParams.bReturnPhysicalMaterial = true;
		QueryParams.AddIgnoredActor(MeshComp->GetOwner());
		return World->LineTraceSingleByChannel(OutResult, BoneLocation, EndTrace, ECC_IK, QueryParams);
	}

	return false;
}
