// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Animation/WSAnimInstance.h"
#include "KismetAnimationLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"

void UWSAnimInstance::NativeInitializeAnimation()
{
    PawnOwner = TryGetPawnOwner();
    bUseMultiThreadedAnimationUpdate = true;
}

void UWSAnimInstance::NativeThreadSafeUpdateAnimation(float DeltaSeconds)
{
    if(!PawnOwner) return;

    bIsFalling = GetIsFalling();

    const auto Velocity = PawnOwner->GetVelocity();
    Speed = Velocity.Size2D();

    Direction = UKismetAnimationLibrary::CalculateDirection(Velocity, PawnOwner->GetActorRotation());
}

bool UWSAnimInstance::GetIsFalling() const
{
    const auto MovementComponent = PawnOwner->FindComponentByClass<UCharacterMovementComponent>();
    return MovementComponent && MovementComponent->IsFalling();
}

void UWSAnimInstance::SetInteractionLoop(UAnimSequence* InLoop)
{
    if(IsValid(InLoop)) { InteractionLoop = InLoop; }
    bIsInteracted = IsValid(InLoop);
}
