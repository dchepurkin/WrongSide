// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "PlayerState/WSPlayerState.h"

#include "AbilitySystem/WSAbilitySystemComponent.h"
#include "AbilitySystem/WSAttributeSet.h"

DEFINE_LOG_CATEGORY_STATIC(LogWSPlayerState, All, All);

AWSPlayerState::AWSPlayerState()
{
	PrimaryActorTick.bCanEverTick = false;

	AbilityComponent = CreateDefaultSubobject<UWSAbilitySystemComponent>("AbilityComponent");
	AbilityComponent->SetIsReplicated(false);

	AttributeSet = CreateDefaultSubobject<UWSAttributeSet>("AttributeSet");
}

void AWSPlayerState::LevelUp()
{
	++Level;
	//TODO LevelUp Delegate
}

void AWSPlayerState::AddXP(const int32 XPToAdd)
{
	XP += FMath::Max(0, XPToAdd);
	//TODO XP changed Delegate
}
