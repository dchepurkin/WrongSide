// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "AbilitySystem/Ability/WSPlayerJumpAbility.h"
#include "GameFramework/Character.h"

DEFINE_LOG_CATEGORY_STATIC(LogWSPlayerJumpAbility, All, All);

UWSPlayerJumpAbility::UWSPlayerJumpAbility()
{
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
}

void UWSPlayerJumpAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	if(!IsValid(Character)) { Character = Cast<ACharacter>(GetAvatarActorFromActorInfo()); }

	if(!IsValid(Character)) { EndAbility(Handle, ActorInfo, ActivationInfo, false, false); }
	if(!Character->CanJump()) { EndAbility(Handle, ActorInfo, ActivationInfo, false, false); }

	Character->LandedDelegate.AddDynamic(this, &ThisClass::OnLanded_Callback);
	Character->Jump();
}

void UWSPlayerJumpAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	if(IsValid(Character))
	{
		Character->LandedDelegate.RemoveAll(this);
	}
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UWSPlayerJumpAbility::OnLanded_Callback_Implementation(const FHitResult& HitResult)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}
