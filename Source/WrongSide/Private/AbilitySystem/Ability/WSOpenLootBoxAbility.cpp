// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "AbilitySystem/Ability/WSOpenLootBoxAbility.h"
#include "Actors/WSLootBox.h"

void UWSOpenLootBoxAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
    if(TriggerEventData)
    {
        LootBox = Cast<AWSLootBox>(TriggerEventData->Target);
    }
    Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
}
