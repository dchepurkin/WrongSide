// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "AbilitySystem/Tasks/WSMovePlayerToTask.h"

#include "NavigationPath.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Utils/WSConstants.h"

UWSMovePlayerToTask* UWSMovePlayerToTask::MovePlayerTo(UGameplayAbility* OwningAbility, FVector InTargetLocation, float InAcceptanceRadius)
{
    if(!IsValid(OwningAbility)) { return nullptr; }

    UWSMovePlayerToTask* NewTask = NewAbilityTask<UWSMovePlayerToTask>(OwningAbility);
    if(IsValid(NewTask))
    {
        NewTask->TargetLocation = InTargetLocation;
        NewTask->AcceptanceRadius = InAcceptanceRadius;// FMath::Max(DefaultAcceptanceDistance, InAcceptanceRadius);
        OwningAbility->OnGameplayAbilityEnded.AddUObject(NewTask, &ThisClass::OnOwnedAbilityEnded_Callback);
    }
    return NewTask;
}

void UWSMovePlayerToTask::Activate()
{
    Pawn = Cast<APawn>(GetAvatarActor());
    if(!IsValid(Pawn)) { FinishTask(); }

    if(InTargetLocation()) { return; }

    Controller = Pawn->GetController();
    if(!IsValid(Controller)) { FinishTask(); }

    UAIBlueprintHelperLibrary::SimpleMoveToLocation(Controller, TargetLocation);
    bTickingTask = true;
}

void UWSMovePlayerToTask::TickTask(float DeltaTime)
{
    Super::TickTask(DeltaTime);

    //Проверяем минимальную дистанцию
    if(InTargetLocation()) { return; }

    //Если не найден путь то отменяем движение к цели
    if(!UAIBlueprintHelperLibrary::GetCurrentPath(Controller))
    {
        FinishTask();
        Fail.Broadcast();
    }
}

void UWSMovePlayerToTask::OnOwnedAbilityEnded_Callback(UGameplayAbility* OwnedAbility)
{
    if(IsValid(this)) { FinishTask(); }
}

bool UWSMovePlayerToTask::InTargetLocation()
{
    constexpr float AcceptanceZOffset = 1.5f * DefaultAcceptanceDistance;
    if(FVector::Dist2D(Pawn->GetActorLocation(), TargetLocation) <= AcceptanceRadius && FVector::Dist(Pawn->GetActorLocation(), TargetLocation) <= AcceptanceRadius + AcceptanceZOffset)
    {
        FinishTask();
        Success.Broadcast();
        return true;
    }
    return false;
}

void UWSMovePlayerToTask::FinishTask()
{
    //GetWorld()->GetTimerManager().ClearTimer(MovingTimerHandle);
    if(IsValid(Controller))
    {
        Controller->StopMovement();
        Controller = nullptr;
    }
    bTickingTask = false;
    MarkAsGarbage();
}
