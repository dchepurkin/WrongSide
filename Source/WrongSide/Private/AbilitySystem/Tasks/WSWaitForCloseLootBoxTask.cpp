// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "AbilitySystem/Tasks/WSWaitForCloseLootBoxTask.h"
#include "UI/Widgets/WSLootBoxWidget.h"
#include "Actors/WSLootBox.h"
#include "Blueprint/UserWidget.h"

UWSWaitForCloseLootBoxTask* UWSWaitForCloseLootBoxTask::WaitForCloseLootBox(UGameplayAbility* OwningAbility, AWSLootBox* InLootBox)
{
    UWSWaitForCloseLootBoxTask* NewTask = NewAbilityTask<UWSWaitForCloseLootBoxTask>(OwningAbility);
    if(NewTask)
    {
        NewTask->Init(InLootBox);
        OwningAbility->OnGameplayAbilityEnded.AddUObject(NewTask, &ThisClass::OnOwnedAbilityEnded_Callback);
    }
    return NewTask;
}

void UWSWaitForCloseLootBoxTask::Init(AWSLootBox* InLootBox)
{
    if(!InLootBox) { FinishTask(); }

    LootBox = InLootBox;

    Controller = InLootBox->GetWorld()->GetFirstPlayerController();

    if(IsValid(Controller))
    {
        const auto& LootBoxWidgetClass = LootBox->GetLootBoxWidgetClass();
        checkf(LootBoxWidgetClass, TEXT("LootBoxWidgetClass is NULL"));

        LootBoxWidget = CreateWidget<UWSLootBoxWidget>(Controller, LootBoxWidgetClass);
        if(LootBoxWidget)
        {
            LootBoxWidget->OnClosed.AddLambda([this]()
            {
                FinishTask();
                Closed.Broadcast();
            });
            LootBoxWidget->AddToViewport();

            FInputModeGameAndUI InputMode;
            InputMode.SetHideCursorDuringCapture(false);
            Controller->SetInputMode(InputMode);
            Controller->SetIgnoreLookInput(true);

            FIntPoint NewMousePosition{};
            Controller->GetViewportSize(NewMousePosition.X, NewMousePosition.Y);
            NewMousePosition /= 2;
            Controller->SetMouseLocation(NewMousePosition.X, NewMousePosition.Y);
            Controller->SetShowMouseCursor(true);
        }
    }
}

void UWSWaitForCloseLootBoxTask::OnOwnedAbilityEnded_Callback(UGameplayAbility* OwningAbility)
{
    if(IsValid(this)) { FinishTask(); }
}

void UWSWaitForCloseLootBoxTask::FinishTask()
{
    if(IsValid(LootBoxWidget))
    {
        LootBoxWidget->RemoveFromParent();
        LootBoxWidget = nullptr;
    }

    if(IsValid(Controller))
    {
        Controller->SetInputMode(FInputModeGameOnly{});
        Controller->SetShowMouseCursor(false);
        Controller->SetIgnoreLookInput(false);
        Controller = nullptr;
    }

    MarkAsGarbage();
}
