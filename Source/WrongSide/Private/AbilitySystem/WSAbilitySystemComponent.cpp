// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "AbilitySystem/WSAbilitySystemComponent.h"
#include "AbilitySystem/Ability/WSAbility.h"
#include "AbilitySystem/Data/WSAbilityData.h"
#include "Utils/WSGameplayTags.h"

void UWSAbilitySystemComponent::BeginPlay()
{
    Super::BeginPlay();

    checkf(StartupMaxVitalAttributes, TEXT("StartupMaxPrimaryAttributes is NULL"));
    checkf(StartupPrimaryAttributes, TEXT("StartupPrimaryAttributes is NULL"));
    checkf(StartupAbilities, TEXT("StartupAbilities is NULL"));

    InitVitalAttributes();
    InitPrimaryAttributes();
    GiveAbilitiesFromData(StartupAbilities);
}

void UWSAbilitySystemComponent::InitVitalAttributes()
{
    ApplyEffectToSelf(StartupMaxVitalAttributes, 1.f);
    ApplyEffectToSelf(StartupVitalAttributes, 1.f);
}

void UWSAbilitySystemComponent::InitPrimaryAttributes()
{
    ApplyEffectToSelf(StartupPrimaryAttributes, 1.f);
}

void UWSAbilitySystemComponent::ApplyEffectToSelf(TSubclassOf<UGameplayEffect> EffectClass, float Level)
{
    auto EffectContextHandle = MakeEffectContext();
    EffectContextHandle.AddSourceObject(GetAvatarActor());
    const auto EffectSpec = MakeOutgoingSpec(EffectClass, Level, EffectContextHandle);
    ApplyGameplayEffectSpecToSelf(*EffectSpec.Data);
}

void UWSAbilitySystemComponent::GiveAbilitiesFromData(UWSAbilityData* AbilityData)
{
    const auto Data = AbilityData->AbilityData;
    for(const auto& AbilityInfo : Data)
    {
        GiveAbilityFromInfo(AbilityInfo);
    }
}

void UWSAbilitySystemComponent::SetActorType(const FGameplayTag& ActorTypeTag)
{
    const auto& TagContainer = GameplayTagCountContainer.GetExplicitGameplayTags();
    if(TagContainer.HasTag(FWSTags::Get().Actor_Type))
    {
        FGameplayTag TagToRemove;
        for(const auto& Tag : TagContainer)
        {
            if(Tag.MatchesTag(FWSTags::Get().Actor_Type))
            {
                TagToRemove = Tag;
                break;
            }
        }
        GameplayTagCountContainer.UpdateTagCount(TagToRemove, -1);
    }

    GameplayTagCountContainer.UpdateTagCount(ActorTypeTag, 1);
}

void UWSAbilitySystemComponent::GiveAbilityFromInfo(const FWSAbilityInfo& AbilityInfo)
{
    checkf(AbilityInfo.AbilityClass, TEXT("Ability class is NULL"));

    FGameplayAbilitySpec AbilitySpec{AbilityInfo.AbilityClass, /*1, INDEX_NONE, GetAvatarActor()*/}; //TODO Maybe remove comments
    AbilitySpec.Ability->AbilityTags.AppendTags(AbilityInfo.AbilityTags);

    if(AbilityInfo.bIsInfinity)
    {
        GiveAbilityAndActivateOnce(AbilitySpec);
        //InfinityAbilities.Add(AbilityInfo.AbilityTag, AbilitySpec); //TODO remove comments if need to remove infinity abilities
    }
    else
    {
        GiveAbility(AbilitySpec);
    }
}

void UWSAbilitySystemComponent::AbilityInputTagPressed(const FGameplayTag& InTag)
{
    AbilityInputTag(InTag, EAbilityGenericReplicatedEvent::InputPressed);
}

void UWSAbilitySystemComponent::AbilityInputTagReleased(const FGameplayTag& InTag)
{
    //AbilityInputTag(InTag, EAbilityGenericReplicatedEvent::InputReleased);

    FScopedAbilityListLock ActiveScopeLock{*this};
    auto& Abilities = GetActivatableAbilities();
    for(auto& AbilitySpec : Abilities)
    {
        if(AbilitySpec.Ability->AbilityTags.HasTagExact(InTag))
        {
            //AbilitySpecInputPressed(AbilitySpec);
            //TryActivateAbility(AbilitySpec.Handle);
            InvokeReplicatedEvent(EAbilityGenericReplicatedEvent::InputReleased, AbilitySpec.Handle, AbilitySpec.ActivationInfo.GetActivationPredictionKey());
        }
    }
}

void UWSAbilitySystemComponent::AbilityInputTag(const FGameplayTag& InTag, EAbilityGenericReplicatedEvent::Type EventType)
{
    FScopedAbilityListLock ActiveScopeLock{*this};
    auto& Abilities = GetActivatableAbilities();
    for(auto& AbilitySpec : Abilities)
    {
        if(AbilitySpec.Ability->AbilityTags.HasTagExact(InTag))
        {
            AbilitySpecInputPressed(AbilitySpec);
            TryActivateAbility(AbilitySpec.Handle);
            InvokeReplicatedEvent(EventType, AbilitySpec.Handle, AbilitySpec.ActivationInfo.GetActivationPredictionKey());
        }
    }
}
