// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "AbilitySystem/WSAbilityFunctionLibrary.h"

#include "AbilitySystem/WSAbilityInterface.h"
#include "AbilitySystem/WSAbilitySystemComponent.h"
#include "AbilitySystem/Ability/WSOpenLootBoxAbility.h"
#include "Actors/WSLootBox.h"
#include "Utils/WSGameplayTags.h"

UWSAbilitySystemComponent* UWSAbilityFunctionLibrary::GetAbilityComponent(AActor* InActor)
{
    const auto WSAbilityInterface = Cast<IWSAbilityInterface>(InActor);
    return WSAbilityInterface ? WSAbilityInterface->GetAbilityComponent() : nullptr;
}

UWSAttributeSet* UWSAbilityFunctionLibrary::GetAttributeSet(AActor* InActor)
{
    const auto WSAbilityInterface = Cast<IWSAbilityInterface>(InActor);
    return WSAbilityInterface ? WSAbilityInterface->GetAttributeSet() : nullptr;
}

bool UWSAbilityFunctionLibrary::TryToActivateAbilityByTag(AActor* InActor, FGameplayTag InTag)
{
    const auto WSAbilityComponent = GetAbilityComponent(InActor);
    return WSAbilityComponent ? WSAbilityComponent->TryActivateAbilitiesByTag(FGameplayTagContainer{InTag}, false) : false;
}

void UWSAbilityFunctionLibrary::TryToOpenLootBox(AActor* InActor, AWSLootBox* InLootBox)
{
    if(const auto WSAbilityComponent = GetAbilityComponent(InActor))
    {
        FScopedAbilityListLock ActiveScopeLock{*WSAbilityComponent};
        const auto& Abilities = WSAbilityComponent->GetActivatableAbilities();
        for(auto& AbilitySpec : Abilities)
        {
            if(AbilitySpec.Ability->AbilityTags.HasTagExact(FWSTags::Get().Ability_Action_OpenLootBox))
            {
                FGameplayEventData EventData;
                EventData.Target = InLootBox;
                WSAbilityComponent->InternalTryActivateAbility(AbilitySpec.Handle, FPredictionKey{}, nullptr, nullptr, &EventData);
            }
        }
    }
}

void UWSAbilityFunctionLibrary::SetAbilityInputTagPressed(AActor* InActor, FGameplayTag InTag)
{
    if(const auto WSAbilityComponent = GetAbilityComponent(InActor))
    {
        WSAbilityComponent->AbilityInputTagPressed(InTag);
    }
}

void UWSAbilityFunctionLibrary::SetAbilityInputTagReleased(AActor* InActor, FGameplayTag InTag)
{
    if(const auto WSAbilityComponent = GetAbilityComponent(InActor))
    {
        WSAbilityComponent->AbilityInputTagReleased(InTag);
    }
}
