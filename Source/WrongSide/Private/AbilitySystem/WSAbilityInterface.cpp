// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "AbilitySystem/WSAbilityInterface.h"
#include "AbilitySystem/WSAbilitySystemComponent.h"

UAbilitySystemComponent* IWSAbilityInterface::GetAbilitySystemComponent() const
{
	return GetAbilityComponent();
}
