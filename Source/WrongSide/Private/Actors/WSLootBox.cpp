// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Actors/WSLootBox.h"
#include "AbilitySystem/WSAbilityFunctionLibrary.h"

AWSLootBox::AWSLootBox()
{
    PrimaryActorTick.bStartWithTickEnabled = false;
    PrimaryActorTick.bCanEverTick = false;

    Body = CreateDefaultSubobject<UStaticMeshComponent>("Body");
    SetRootComponent(Body);

    Head = CreateDefaultSubobject<UStaticMeshComponent>("head");
    Head->SetupAttachment(GetRootComponent());
}

void AWSLootBox::BeginPlay()
{
    Super::BeginPlay();

    InteractionInfo.InteractionPosition = GetActorLocation();
}

FWSInteractionActorInfo AWSLootBox::GetInteractionActorInfo_Implementation()
{
    return InteractionInfo;
}

UAnimMontage* AWSLootBox::GetInteractionAnimation_Implementation(AActor* Interactor) const
{
    return OpenAnimation;
}

void AWSLootBox::Interact_Implementation(AActor* Interactor)
{
    Open(Interactor);
}

void AWSLootBox::Open(AActor* WhoOpen)
{
    UWSAbilityFunctionLibrary::TryToOpenLootBox(WhoOpen, this);
}
