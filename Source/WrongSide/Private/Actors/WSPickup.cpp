// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Actors/WSPickup.h"

DEFINE_LOG_CATEGORY_STATIC(LogWSPickup, All, All);

AWSPickup::AWSPickup()
{
    PrimaryActorTick.bStartWithTickEnabled = false;
    PrimaryActorTick.bCanEverTick = false;
}

void AWSPickup::Take(AActor* WhoTake)
{
    if(!IsValid(WhoTake)) { return; }
    //TODO AddItem To Inventory
}

void AWSPickup::BeginPlay()
{
    Super::BeginPlay();
}

FWSInteractionActorInfo AWSPickup::GetInteractionActorInfo_Implementation()
{
    FWSInteractionActorInfo Info = InteractionInfo;
    Info.InteractionPosition = GetActorLocation();

    return Info;
}

void AWSPickup::Interact_Implementation(AActor* Interactor)
{
    Take(Interactor);
}

UAnimMontage* AWSPickup::GetInteractionAnimation_Implementation(AActor* Interactor) const
{
    return GetTakeMontageByHeight(CalculateHeight(Interactor));
}

float AWSPickup::CalculateHeight(AActor* InOtherActor) const
{
    if(!IsValid(InOtherActor)) { return 0.f; }

    const auto PickUpLocation = GetActorLocation();
    const auto OtherActorLocation = InOtherActor->GetActorLocation();

    return PickUpLocation.Z - OtherActorLocation.Z;
}

UAnimMontage* AWSPickup::GetTakeMontageByHeight(float InHeight) const
{
    if(InHeight < TakeDownHeightThreshold) { return TakeDown; }
    if(InHeight < TakeDownHeight_100_Threshold) { return TakeMiddle_100; }
    if(InHeight < TakeDownHeight_150_Threshold) { return TakeMiddle_150; }
    return TakeMiddle_200;
}
