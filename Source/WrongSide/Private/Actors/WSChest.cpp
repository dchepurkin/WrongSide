// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Actors/WSChest.h"

#include "Components/WSLockComponent.h"

AWSChest::AWSChest()
{
    PrimaryActorTick.bStartWithTickEnabled = false;
    PrimaryActorTick.bCanEverTick = false;

    InteractionPosition = CreateDefaultSubobject<USceneComponent>("InteractionPosition");
    InteractionPosition->SetupAttachment(GetRootComponent());

    Lock = CreateDefaultSubobject<UWSLockComponent>("Lock");
}

void AWSChest::BeginPlay()
{
    Super::BeginPlay();

    if(IsValid(InteractionPosition))
    {
        InteractionInfo.InteractionPosition = InteractionPosition->GetComponentLocation();
    }
    if(IsValid(Lock))
    {
        Lock->SetLockInfo(bIsClosed, LockInfo);
    }
}

bool AWSChest::CanInteract_Implementation(AActor* Interactor, FText& FailMessage)
{
    return IsValid(Lock) ? Lock->CanOpen(Interactor, FailMessage) : false;
}

void AWSChest::Interact_Implementation(AActor* Interactor)
{
    Open(Interactor);
}
