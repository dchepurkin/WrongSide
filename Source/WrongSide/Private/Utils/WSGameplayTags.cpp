// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Utils/WSGameplayTags.h"
#include "GameplayTagsManager.h"

FWSTags FWSTags::WSTags;

#define CREATE_TAG(TagName) UGameplayTagsManager::Get().AddNativeGameplayTag(FName(TagName));

void FWSTags::InitTags()
{
    WSTags.InitBodyPartsTags();
    WSTags.InitInputTags();
    WSTags.InitPlayerTags();
    WSTags.InitActorTags();
    WSTags.InitAttributesTags();
    WSTags.InitAbilityTags();
    WSTags.InitEventTags();
}

void FWSTags::InitBodyPartsTags()
{
    BodyPart_Head = CREATE_TAG("BodyPart.Head");
    BodyPart_Hair = CREATE_TAG("BodyPart.Hair");
    BodyPart_Eyebrows = CREATE_TAG("BodyPart.Eyebrows");
    BodyPart_Tors = CREATE_TAG("BodyPart.Tors");
}

void FWSTags::InitInputTags()
{
    Input_Look = CREATE_TAG("Input.Look");
    Input_Jump = CREATE_TAG("Input.Jump");
    Input_Move = CREATE_TAG("Input.Move");
    Input_Move = CREATE_TAG("Input.Interact");
    Input_Sprint = CREATE_TAG("Input.Sprint");
}

void FWSTags::InitPlayerTags()
{
    Player = CREATE_TAG("Player");
    Player_Navigation = CREATE_TAG("Player.Navigation");
    Player_Navigation_MoveTo = CREATE_TAG("Player.Navigation.MoveTo");
    Player_Navigation_StopMoving = CREATE_TAG("Player.Navigation.StopMoving");
    Player_Navigation_MovingFinished = CREATE_TAG("Player.Navigation.MovingFinished");
}

void FWSTags::InitActorTags()
{
    Actor = CREATE_TAG("Actor");
    Actor_Type = CREATE_TAG("Actor.Type");
    Actor_Type_Item = CREATE_TAG("Actor.Type.Item");
    Actor_Type_None = CREATE_TAG("Actor.Type.None");
    Actor_Type_Player = CREATE_TAG("Actor.Type.Player");
    Actor_Type_Enemy = CREATE_TAG("Actor.Type.Enemy");
    Actor_Type_Enemy_Aggressive = CREATE_TAG("Actor.Type.Enemy.Aggressive");
    Actor_Type_Enemy_Neutral = CREATE_TAG("Actor.Type.Enemy.Neutral");
    Actor_Type_NPC = CREATE_TAG("Actor.Type.NPC");
}

void FWSTags::InitAttributesTags()
{
    Attributes_Primary_Health = CREATE_TAG("Attributes.Primary.Health");
    Attributes_Primary_MaxHealth = CREATE_TAG("Attributes.Primary.MaxHealth");
    Attributes_Primary_Mana = CREATE_TAG("Attributes.Primary.Mana");
    Attributes_Primary_MaxMana = CREATE_TAG("Attributes.Primary.MaxMana");
    Attributes_Primary_Stamina = CREATE_TAG("Attributes.Primary.Stamina");
    Attributes_Primary_MaxStamina = CREATE_TAG("Attributes.Primary.MaxStamina");
}

void FWSTags::InitAbilityTags()
{
    Ability = CREATE_TAG("Ability");
    Ability_Action = CREATE_TAG("Ability.Action");
    Ability_Action_OpenLootBox = CREATE_TAG("Ability.Action.OpenLootBox");
    Ability_Action_LockPicking = CREATE_TAG("Ability.Action.LockPicking");
    Ability_Action_Sprint = CREATE_TAG("Ability.Action.Sprint");
    Ability_Action_Jump = CREATE_TAG("Ability.Action.Jump");
    Ability_Action_Moving = CREATE_TAG("Ability.Action.Moving");
    Ability_Action_Interaction = CREATE_TAG("Ability.Action.Interaction");
    Ability_Debuff_Infection = CREATE_TAG("Ability.Debuff.Infection");
    Ability_Debuff_Infection_Poison = CREATE_TAG("Ability.Debuff.Infection.Poison")
}

void FWSTags::InitEventTags()
{
    Event = CREATE_TAG("Event");
    Event_Interaction = CREATE_TAG("Event.Interaction")
    Event_Interaction_Started = CREATE_TAG("Event.Interaction.Started");
    Event_Interaction_Finished = CREATE_TAG("Event.Interaction.Finished");
    Event_ScreenMessage = CREATE_TAG("Event.ScreenMessage");
}
