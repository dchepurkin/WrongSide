// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "FunctionLibraries/WSFunctionLibrary.h"

#include "MotionWarpingComponent.h"
#include "Animation/WSAnimInstance.h"
#include "Components/WSInteractionComponent.h"
#include "GameFramework/Character.h"
#include "Interfaces/WSGameplayTagInterface.h"
#include "Interfaces/WSInteractionInterface.h"
#include "Interfaces/WSPlayerInterface.h"
#include "Utils/WSConstants.h"
#include "Utils/WSGameplayTags.h"

bool UWSFunctionLibrary::IsEnemy(AActor* InActor)
{
    if(!InActor) { return false; }

    if(const auto TagAssetInterface = Cast<IWSGameplayTagInterface>(InActor))
    {
        return TagAssetInterface->HasMatchingGameplayTag(FWSTags::Get().Actor_Type_Enemy);
    }

    return false;
}

void UWSFunctionLibrary::SetActorType(AActor* InActor, FGameplayTag Type)
{
    if(!InActor || !Type.IsValid() || !Type.MatchesTag(FWSTags::Get().Actor_Type)) { return; }

    if(const auto TagAssetInterface = Cast<IWSGameplayTagInterface>(InActor))
    {
        return TagAssetInterface->SetActorType(Type);
    }
}

void UWSFunctionLibrary::WarpToLocation(AActor* InActor, const FVector& InLocation)
{
    if(const auto MotionWarpingComponent = InActor ? InActor->FindComponentByClass<UMotionWarpingComponent>() : nullptr)
    {
        MotionWarpingComponent->AddOrUpdateWarpTargetFromLocation(DefaultWarpTargetName, InLocation);
    }
}

void UWSFunctionLibrary::SetInteractionLoopAnimation(AActor* InActor, AActor* InteractedActor)
{
    if(const auto Character = Cast<ACharacter>(InActor); Character->GetMesh())
    {
        if(const auto WSAnimInstance = Cast<UWSAnimInstance>(Character->GetMesh()->GetAnimInstance()))
        {
            WSAnimInstance->SetInteractionLoop(InteractedActor && InteractedActor->Implements<UWSInteractionInterface>() ? IWSInteractionInterface::Execute_GetInteractionLoopAnimation(InteractedActor) : nullptr);
        }
    }
}

void UWSFunctionLibrary::BreakInteractionLoopAnimation(AActor* InActor)
{
    SetInteractionLoopAnimation(InActor, nullptr);
}

void UWSFunctionLibrary::SetOutlineEnabled(AActor* InActor, bool IsEnabled)
{
    if(!IsValid(InActor)) { return; }

    TArray<UMeshComponent*> Meshes;
    InActor->GetComponents<UMeshComponent>(Meshes);

    for(const auto Mesh : Meshes)
    {
        if(!Mesh) continue;
        Mesh->SetRenderCustomDepth(IsEnabled);
    }
}

UWSInteractionComponent* UWSFunctionLibrary::GetPlayerInteractionComponent(AActor* InPlayerActor)
{
    const auto PlayerInterface = Cast<IWSPlayerInterface>(InPlayerActor);
    return PlayerInterface ? PlayerInterface->GetInteractionComponent() : nullptr;
}

void UWSFunctionLibrary::SetPlayerFocusEnabled(AActor* InPlayerActor, bool IsEnabled)
{
    if(const auto InteractionComponent = GetPlayerInteractionComponent(InPlayerActor))
    {
        InteractionComponent->SetFocusActive(IsEnabled);
    }
}

AActor* UWSFunctionLibrary::GetPlayerFocusedActor(AActor* InPlayerActor)
{
    const auto InteractionComponent = GetPlayerInteractionComponent(InPlayerActor);
    return InteractionComponent ? InteractionComponent->GetFocusedActor() : nullptr;
}
