// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Controller/WSPlayerController.h"
#include "Input/WSInputAction.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "AbilitySystem/WSAbilityFunctionLibrary.h"
#include "Interfaces/WSPlayerInterface.h"

DEFINE_LOG_CATEGORY_STATIC(LogWSPlayerController, All, All);

void AWSPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	checkf(InPawn && InPawn->Implements<UWSPlayerInterface>(), TEXT("PlayerInterface is not implemented"));

	if(!IsValid(PlayerCameraManager)) return;

	DisableInput(this);
	PlayerCameraManager->SetManualCameraFade(1.f, FLinearColor::Black, false);
	GetWorldTimerManager().SetTimer(CameraFadeTimerHandle, this, &ThisClass::CameraFadeTimer_Callback, CameraFadeTime);
}

void AWSPlayerController::BeginPlay()
{
	Super::BeginPlay();
	SetInputMode(FInputModeGameOnly{});
	SetShowMouseCursor(false);
}

void AWSPlayerController::CameraFadeTimer_Callback()
{
	EnableInput(this);
	if(!PlayerCameraManager) return;
	PlayerCameraManager->StartCameraFade(1.f, 0.f, CameraFadeOutTime, FLinearColor::Black);
}

void AWSPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	checkf(DefaultMapping, TEXT("DefaultMapping is NULL"));
	checkf(LookAction, TEXT("LookAction is NULL"));
	checkf(MoveAction, TEXT("MoveAction is NULL"));
	checkf(JumpAction, TEXT("JumpAction is NULL"));
	checkf(InteractAction, TEXT("InteractAction is NULL"));
	checkf(SprintAction, TEXT("SprintAction is NULL"));

	SetInputMapping(DefaultMapping);

	if(const auto EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent))
	{
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ThisClass::OnLook_Callback);
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ThisClass::OnMove_Callback);
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Started, this, &ThisClass::TagInputPressed_Callback, MoveAction->InputTag);
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Completed, this, &ThisClass::TagInputReleased_Callback, MoveAction->InputTag);
		EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Started, this, &ThisClass::TagInputPressed_Callback, SprintAction->InputTag);
		EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Completed, this, &ThisClass::TagInputReleased_Callback, SprintAction->InputTag);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ThisClass::TagInputPressed_Callback, JumpAction->InputTag);
		EnhancedInputComponent->BindAction(InteractAction, ETriggerEvent::Started, this, &ThisClass::TagInputPressed_Callback, InteractAction->InputTag);
	}
}

void AWSPlayerController::SetInputMapping(UInputMappingContext* InMappingContext) const
{
	if(const auto EnhancedInputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		EnhancedInputSubsystem->ClearAllMappings();
		EnhancedInputSubsystem->AddMappingContext(InMappingContext, 0);
	}
}

void AWSPlayerController::OnLook_Callback(const FInputActionValue& Value)
{
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	AddYawInput(LookAxisVector.X);
	AddPitchInput(LookAxisVector.Y);
}

void AWSPlayerController::OnMove_Callback(const FInputActionValue& Value)
{
	if(const auto ControlledPawn = GetPawn())
	{
		const FVector2D MovementVector = Value.Get<FVector2D>();

		const FRotator Rotation = GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		ControlledPawn->AddMovementInput(ForwardDirection, MovementVector.Y);
		ControlledPawn->AddMovementInput(RightDirection, MovementVector.X);
	}
}

void AWSPlayerController::TagInputPressed_Callback(const FGameplayTag InTag)
{
	UWSAbilityFunctionLibrary::SetAbilityInputTagPressed(GetPawn(), InTag);
}

void AWSPlayerController::TagInputReleased_Callback(const FGameplayTag InTag)
{
	UWSAbilityFunctionLibrary::SetAbilityInputTagReleased(GetPawn(), InTag);
}
