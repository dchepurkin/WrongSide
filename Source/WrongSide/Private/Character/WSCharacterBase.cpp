// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Character/WSCharacterBase.h"

#include "MotionWarpingComponent.h"
#include "AbilitySystem/WSAbilityFunctionLibrary.h"
#include "AbilitySystem/WSAbilitySystemComponent.h"
#include "Utils/WSGameplayTags.h"

DEFINE_LOG_CATEGORY_STATIC(LogWSCharacterBase, All, All);

AWSCharacterBase::AWSCharacterBase(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    PrimaryActorTick.bCanEverTick = false;

    GetMesh()->bEnableUpdateRateOptimizations = true;
    GetMesh()->SetRelativeRotation(FRotator{0.f, -90.f, 0.f});

    MotionWarpingComponent = CreateDefaultSubobject<UMotionWarpingComponent>("MotionWarpingComponent");
}

void AWSCharacterBase::BeginPlay()
{
    Super::BeginPlay();

    SetActorType(FWSTags::Get().Actor_Type_None);
}

void AWSCharacterBase::GetOwnedGameplayTags(FGameplayTagContainer& InTagContainer) const
{
    if(AbilityComponent) { AbilityComponent->GetOwnedGameplayTags(InTagContainer); }
}

void AWSCharacterBase::SetActorType(FGameplayTag ActorTypeTag)
{
    if(AbilityComponent) { AbilityComponent->SetActorType(ActorTypeTag); }
    //TODO Maybe need broadcast delegate
}


