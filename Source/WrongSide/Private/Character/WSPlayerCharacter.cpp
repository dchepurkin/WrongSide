// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "Character/WSPlayerCharacter.h"

#include "NavigationInvokerComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/WSCameraComponent.h"
#include "AbilitySystem/WSAbilitySystemComponent.h"
#include "AbilitySystem/WSAttributeSet.h"
#include "Components/WSCharacterMovementComponent.h"
#include "Components/WSInteractionComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "PlayerState/WSPlayerState.h"
#include "Utils/WSConstants.h"
#include "Utils/WSGameplayTags.h"

DEFINE_LOG_CATEGORY_STATIC(LogWSPlayerCharacter, All, All);

AWSPlayerCharacter::AWSPlayerCharacter(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer.SetDefaultSubobjectClass(CharacterMovementComponentName, UWSCharacterMovementComponent::StaticClass()))
{
    PrimaryActorTick.bCanEverTick = false;
    PrimaryActorTick.bStartWithTickEnabled = false;

    bUseControllerRotationYaw = false;
    bUseControllerRotationPitch = false;
    bUseControllerRotationRoll = false;

    GetCapsuleComponent()->SetCollisionProfileName(PlayerCollisionProfile);

    GetMesh()->SetCollisionProfileName(PlayerMeshCollisionProfile);
    GetMesh()->SetRelativeLocation(FVector{0.f, 0.f, -91.f});

    HeadMesh = CreateBodyPartMesh("HeadMesh", FWSTags::Get().BodyPart_Head);
    HairMesh = CreateBodyPartMesh("HairMesh", FWSTags::Get().BodyPart_Hair);
    EyebrowsMesh = CreateBodyPartMesh("EyebrowsMesh", FWSTags::Get().BodyPart_Eyebrows);
    SetBodyPartTag(GetMesh(), FWSTags::Get().BodyPart_Tors);

    SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
    SpringArm->SetupAttachment(GetRootComponent());
    SpringArm->TargetArmLength = 650.f;
    SpringArm->SetRelativeLocation(FVector(0.f, 0.f, 80.f));
    SpringArm->bUsePawnControlRotation = true;
    SpringArm->bEnableCameraLag = true;
    SpringArm->CameraLagSpeed = 20.f;
    SpringArm->bEnableCameraRotationLag = true;
    SpringArm->CameraRotationLagSpeed = 15.f;
    SpringArm->SocketOffset = FVector(0.f, 0.f, 100.f);

    Camera = CreateDefaultSubobject<UWSCameraComponent>("Camera");
    Camera->SetupAttachment(SpringArm);
    Camera->SetRelativeRotation(FRotator(-5.f, 0.f, 0.f));
    Camera->FieldOfView = 75.f;

    NavigationInvokerComponent = CreateDefaultSubobject<UNavigationInvokerComponent>("NavigationInvokerComponent");
    NavigationInvokerComponent->SetGenerationRadii(600.f, 600.f);

    InteractionComponent = CreateDefaultSubobject<UWSInteractionComponent>("InteractionComponent");
    InteractionComponent->SetupAttachment(GetRootComponent());
    InteractionComponent->SetRelativeLocation(FVector(70.f, 0.f, -86.f));
    InteractionComponent->SetSphereRadius(70.f);
    InteractionComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

void AWSPlayerCharacter::PossessedBy(AController* NewController)
{
    Super::PossessedBy(NewController);

    InitAbilityActorInfo();
}

void AWSPlayerCharacter::BeginPlay()
{
    Super::BeginPlay();

    SetActorType(FWSTags::Get().Actor_Type_Player);

    if(Camera)
    {
        Camera->OnCameraBeginOverlap.AddUObject(this, &ThisClass::OnCameraBeginOverlap_Callback);
        Camera->OnCameraEndOverlap.AddUObject(this, &ThisClass::OnCameraEndOverlap_Callback);
    }
}

void AWSPlayerCharacter::OnCameraBeginOverlap_Callback() const
{
    if(GetMesh()) GetMesh()->SetVisibility(false, true);
}

void AWSPlayerCharacter::OnCameraEndOverlap_Callback() const
{
    if(GetMesh()) GetMesh()->SetVisibility(true, true);
}

USkeletalMeshComponent* AWSPlayerCharacter::CreateBodyPartMesh(const FName& InBodyPartName, const FGameplayTag& InTag)
{
    const auto NewMesh = CreateDefaultSubobject<USkeletalMeshComponent>(InBodyPartName);
    NewMesh->SetupAttachment(GetMesh());
    NewMesh->SetLeaderPoseComponent(GetMesh());
    NewMesh->SetCollisionProfileName(PlayerMeshCollisionProfile);
    NewMesh->bEnableUpdateRateOptimizations = true;
    SetBodyPartTag(NewMesh, InTag);

    return NewMesh;
}

void AWSPlayerCharacter::SetBodyPartTag(USkeletalMeshComponent* InMesh, const FGameplayTag& InTag)
{
    InMesh->ComponentTags.Add(FName{InTag.ToString()});
    BodyPartsMeshes.Add(InTag, InMesh);
}

USkeletalMeshComponent* AWSPlayerCharacter::GetBodyPartByTag(FGameplayTag InTag) const
{
    return BodyPartsMeshes.Contains(InTag) ? BodyPartsMeshes[InTag] : nullptr;
}

void AWSPlayerCharacter::InitAbilityActorInfo()
{
    if(const auto WSPlayerState = GetPlayerState<AWSPlayerState>())
    {
        AbilityComponent = WSPlayerState->GetAbilityComponent();
        AttributeSet = WSPlayerState->GetAttributeSet();

        if(AbilityComponent)
        {
            AbilityComponent->InitAbilityActorInfo(WSPlayerState, this);
        }
    }
}
