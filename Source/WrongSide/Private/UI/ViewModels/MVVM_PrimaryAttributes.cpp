// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "UI/ViewModels/MVVM_PrimaryAttributes.h"

#include "AbilitySystem/WSAbilityInterface.h"
#include "AbilitySystem/WSAbilitySystemComponent.h"
#include "AbilitySystem/WSAttributeSet.h"
#include "Utils/WSGameplayTags.h"

void UMVVM_PrimaryAttributes::InitBindings(AActor* InActor)
{
	const auto AbilityInterface = Cast<IWSAbilityInterface>(InActor);
	if(!AbilityInterface) { return; }

	const auto AbilityComponent = AbilityInterface->GetAbilityComponent();
	if(!IsValid(AbilityComponent)) { return; }

	const auto AttributeSet = AbilityInterface->GetAttributeSet();
	if(!IsValid(AttributeSet)) { return; }

	AbilityComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetHealthAttribute()).AddUObject(this, &ThisClass::SetHealth);
	AbilityComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMaxHealthAttribute()).AddUObject(this, &ThisClass::SetMaxHealth);
	AbilityComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetStaminaAttribute()).AddUObject(this, &ThisClass::SetStamina);
	AbilityComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMaxStaminaAttribute()).AddUObject(this, &ThisClass::SetMaxStamina);
	AbilityComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetManaAttribute()).AddUObject(this, &ThisClass::SetMana);
	AbilityComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMaxManaAttribute()).AddUObject(this, &ThisClass::SetMaxMana);
	AbilityComponent->RegisterGameplayTagEvent(FWSTags::Get().Ability_Debuff_Infection, EGameplayTagEventType::NewOrRemoved).AddUObject(this, &ThisClass::SetInfected);

	NotifyStartupValues(AttributeSet);
}

void UMVVM_PrimaryAttributes::SetHealth(const FOnAttributeChangeData& ValueData)
{
	UE_MVVM_SET_PROPERTY_VALUE(Health, ValueData.NewValue);
	SetHealthPercent();
}

void UMVVM_PrimaryAttributes::SetMaxHealth(const FOnAttributeChangeData& ValueData)
{
	UE_MVVM_SET_PROPERTY_VALUE(MaxHealth, ValueData.NewValue);
	SetHealthPercent();
}

void UMVVM_PrimaryAttributes::SetHealthPercent()
{
	UE_MVVM_SET_PROPERTY_VALUE(HealthPercent, FMath::IsNearlyZero(MaxHealth) ? 0.f : Health / MaxHealth);
}

void UMVVM_PrimaryAttributes::SetStamina(const FOnAttributeChangeData& ValueData)
{
	UE_MVVM_SET_PROPERTY_VALUE(Stamina, ValueData.NewValue);
	SetStaminaPercent();
}

void UMVVM_PrimaryAttributes::SetMaxStamina(const FOnAttributeChangeData& ValueData)
{
	UE_MVVM_SET_PROPERTY_VALUE(MaxStamina, ValueData.NewValue);
	SetStaminaPercent();
}

void UMVVM_PrimaryAttributes::SetStaminaPercent()
{
	UE_MVVM_SET_PROPERTY_VALUE(StaminaPercent, FMath::IsNearlyZero(MaxStamina) ? 0.f : Stamina / MaxStamina);
}

void UMVVM_PrimaryAttributes::SetMana(const FOnAttributeChangeData& ValueData)
{
	UE_MVVM_SET_PROPERTY_VALUE(Mana, ValueData.NewValue);
	SetManaPercent();
}

void UMVVM_PrimaryAttributes::SetMaxMana(const FOnAttributeChangeData& ValueData)
{
	UE_MVVM_SET_PROPERTY_VALUE(MaxMana, ValueData.NewValue);
	SetManaPercent();
}

void UMVVM_PrimaryAttributes::SetManaPercent()
{
	UE_MVVM_SET_PROPERTY_VALUE(ManaPercent, FMath::IsNearlyZero(MaxMana) ? 0.f : Mana / MaxMana);
}

void UMVVM_PrimaryAttributes::SetInfected(const FGameplayTag Tag, int32 Count)
{
	UE_MVVM_SET_PROPERTY_VALUE(bInfected, Count > 0);
}

void UMVVM_PrimaryAttributes::NotifyStartupValues(UWSAttributeSet* AttributeSet)
{
	if(!AttributeSet) { return; }

	UE_MVVM_SET_PROPERTY_VALUE(MaxHealth, AttributeSet->GetMaxHealth());
	UE_MVVM_SET_PROPERTY_VALUE(Health, AttributeSet->GetHealth());
	UE_MVVM_SET_PROPERTY_VALUE(MaxStamina, AttributeSet->GetMaxStamina());
	UE_MVVM_SET_PROPERTY_VALUE(Stamina, AttributeSet->GetStamina());
	UE_MVVM_SET_PROPERTY_VALUE(MaxMana, AttributeSet->GetMaxMana());
	UE_MVVM_SET_PROPERTY_VALUE(Mana, AttributeSet->GetMana());
	SetHealthPercent();
	SetStaminaPercent();
	SetManaPercent();
}
