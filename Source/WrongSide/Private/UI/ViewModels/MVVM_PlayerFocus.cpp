// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "UI/ViewModels/MVVM_PlayerFocus.h"

#include "Components/WSInteractionComponent.h"

void UMVVM_PlayerFocus::SetIsFocused(bool IsFocused)
{
	UE_MVVM_SET_PROPERTY_VALUE(bIsFocused, IsFocused);
}

void UMVVM_PlayerFocus::SetFocusedActor(AActor* InActor)
{
	UE_MVVM_SET_PROPERTY_VALUE(FocusedActor, InActor);
}

void UMVVM_PlayerFocus::InitBindings(UWSInteractionComponent* InteractionComponent)
{
	if(IsValid(InteractionComponent))
	{
		InteractionComponent->OnActorFocused.AddDynamic(this, &ThisClass::OnActorFocused_Callback);
		InteractionComponent->OnClearFocus.AddDynamic(this, &ThisClass::OnClearFocus_Callback);
	}
}

void UMVVM_PlayerFocus::OnActorFocused_Callback(AActor* InActor)
{
	SetFocusedActor(InActor);
	SetIsFocused(IsValid(InActor));
}

void UMVVM_PlayerFocus::OnClearFocus_Callback()
{
	SetIsFocused(false);
}
