// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "UI/Widgets/WSLootBoxWidget.h"

void UWSLootBoxWidget::Close()
{
    OnClosed.Broadcast();
    RemoveFromParent();
}
