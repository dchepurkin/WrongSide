// Created by Dmitriy Chepurkin. All assets are used for educational purposes

#include "UI/HUD/WSHUD.h"

#include "UI/Widgets/WSWidget.h"

DEFINE_LOG_CATEGORY_STATIC(LogWSHUD, All, All);

void AWSHUD::BeginPlay()
{
	Super::BeginPlay();

	checkf(GameplayScreenClass, TEXT("GameplayScreenClass is NULL"));

	GameplayScreenWidget = CreateWidget<UWSWidget>(GetOwningPlayerController(), GameplayScreenClass);
	GameplayScreenWidget->AddToViewport();
}
