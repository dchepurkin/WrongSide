// Created by Dmitriy Chapurkin. All assets are used for educational purposes

using UnrealBuildTool;
using System.Collections.Generic;

public class WrongSideEditorTarget : TargetRules
{
	public WrongSideEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V4;

		ExtraModuleNames.AddRange( new string[] { "WrongSide" } );
	}
}
